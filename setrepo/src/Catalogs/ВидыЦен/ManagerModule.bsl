#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Определяет вид цены по умолчанию
//
// Параметры:
//  ВидЦены - СправочникСсылка.ВидыЦен, Неопределено
//  Отборы  - Структура - (необязательный)
//                         Структура отборов, где Ключ - имя реквизита, Значение - значение реквизита
//
// Возвращаемое значение:
//  СправочникСсылка.ВидыЦен - Найденный вид цен
//
Функция ВидЦеныПоУмолчанию(Знач ВидЦены, Отборы = Неопределено) Экспорт
	
	Если ЗначениеЗаполнено(ВидЦены) Тогда
		Возврат ВидЦены;
	КонецЕсли;
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 2
	|	ВидыЦен.Ссылка КАК ВидЦены
	|ИЗ
	|	Справочник.ВидыЦен КАК ВидыЦен
	|ГДЕ
	|	НЕ ВидыЦен.ПометкаУдаления
	|");
	
	Если Отборы <> Неопределено Тогда
		Для Каждого Отбор Из Отборы Цикл
			Запрос.Текст = Запрос.Текст + "
				|	И ВидыЦен." + Отбор.Ключ + " = &" + Отбор.Ключ;
			Запрос.УстановитьПараметр(Отбор.Ключ, Отбор.Значение);
		КонецЦикла;
	КонецЕсли;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Количество() = 1 И Выборка.Следующий() Тогда
		ВидЦены = Выборка.ВидЦены;
	Иначе
		ВидЦены = Справочники.ВидыЦен.ПустаяСсылка();
	КонецЕсли;
	
	Возврат ВидЦены;
	
КонецФункции

// Возвращает массив видов цен, доступных пользователю
//
// Возвращаемое значение:
//  Массив - виды цен, разрешенные пользователю
//
Функция ДоступныеВидыЦен(Отборы = Неопределено) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	ВидыЦен.Ссылка КАК ВидЦены
	|ИЗ
	|	Справочник.ВидыЦен КАК ВидыЦен
	|ГДЕ
	|	ИСТИНА
	|");
	
	Если Отборы <> Неопределено Тогда
		Для каждого Отбор Из Отборы Цикл
			Запрос.Текст = Запрос.Текст + Символы.ПС + "	И ВидыЦен." + Отбор.Ключ + " = &" + Отбор.Ключ;
			Запрос.УстановитьПараметр(Отбор.Ключ, Отбор.Значение);
		КонецЦикла;
	КонецЕсли;
	
	РезультатЗапроса = Запрос.Выполнить();
	Возврат РезультатЗапроса.Выгрузить().ВыгрузитьКолонку("ВидЦены");
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБНОВЛЕНИЕ ИНФОРМАЦИОННОЙ БАЗЫ
#Область ОбновлениеИнформационнойБазы

// Заполняет реквизиты справочника расчетными значениями
//
Процедура ЗаполнитьВидыЦен() Экспорт
	
	ЗаполнитьВариантОкругления();
	ЗаполнитьИдентификатор();
	ЗаполнитьФормулуИНаценку();
	
КонецПроцедуры

Процедура ЗаполнитьВариантОкругления()
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Ссылка
	|ИЗ
	|	Справочник.ВидыЦен
	|ГДЕ
	|	ВариантОкругления = ЗНАЧЕНИЕ(Перечисление.ВариантыОкругления.ПустаяСсылка)
	|");
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Объект = Выборка.Ссылка.ПолучитьОбъект();
		
		Если Объект.ОкруглятьВБольшуюСторону Тогда
			Объект.ВариантОкругления = Перечисления.ВариантыОкругления.ВсегдаВБольшуюСторону;
		Иначе
			Объект.ВариантОкругления = Перечисления.ВариантыОкругления.ПоАрифметическимПравилам;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьИдентификатор()
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Ссылка,
	|	Наименование
	|ИЗ
	|	Справочник.ВидыЦен
	|ГДЕ
	|	Идентификатор = """"
	|");
	
	ИдентификаторыВидовЦен = Новый Структура;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Счетчик = 0;
		Идентификатор = ОбщегоНазначенияБольничнаяАптекаКлиентСервер.ПолучитьИдентификатор(Выборка.Наименование);
		Пока ИдентификаторыВидовЦен.Свойство(Идентификатор) Цикл
			Идентификатор = Идентификатор + Формат(Счетчик, "ЧРГ=' '; ЧГ=0");
		КонецЦикла;
		
		ИдентификаторыВидовЦен.Вставить(Идентификатор, Выборка.Ссылка);
		
	КонецЦикла;
	
	Для Каждого КлючЗначение Из ИдентификаторыВидовЦен Цикл
		
		Объект = КлючЗначение.Значение.ПолучитьОбъект();
		Объект.Идентификатор = КлючЗначение.Ключ;
		
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьФормулуИНаценку()
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Ссылка
	|ИЗ
	|	Справочник.ВидыЦен.ПроцентыПоДиапазонам
	|ГДЕ
	|	Ссылка.СпособЗаданияЦены = ЗНАЧЕНИЕ(Перечисление.СпособыЗаданияЦен.НаценкаНаДругойВидЦен)
	|");
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Объект = Выборка.Ссылка.ПолучитьОбъект();
		
		ВлияющийВидЦены = Объект.ВлияющиеВидыЦен.Добавить();
		ВлияющийВидЦены.ВидЦены = Объект.БазовыйВидЦены;
		
		Если Объект.ПроцентыПоДиапазонам.Количество() = 1 Тогда
			Объект.Наценка = Объект.ПроцентыПоДиапазонам[0].Процент;
		Иначе
			Формула = "%1";
			Идентификатор = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.БазовыйВидЦены, "Идентификатор");
			МаксимальныйИндекс = Объект.ПроцентыПоДиапазонам.Количество() - 1;
			Для Индекс = 0 По МаксимальныйИндекс - 1 Цикл
				
				ТекущаяСтрока = Объект.ПроцентыПоДиапазонам[МаксимальныйИндекс - Индекс];
				Формула = СтрШаблон(Формула, "?([" + Идентификатор + "] > " + Формат(ТекущаяСтрока.НижняяГраницаДиапазонаЦен, "ЧРД=.; ЧГ=0") + ", ["+ Идентификатор + "] * (" + Формат(1 + ТекущаяСтрока.Процент / 100, "ЧРД=.; ЧГ=0") + "), %1)");
				
			КонецЦикла;
			
			ТекущаяСтрока = Объект.ПроцентыПоДиапазонам[0];
			Формула = СтрШаблон(Формула, "["+ Идентификатор + "] * (" + Формат(1 + ТекущаяСтрока.Процент / 100, "ЧРД=.; ЧГ=0") + ")");
			
			Объект.СпособЗаданияЦены = Перечисления.СпособыЗаданияЦен.РассчитыватьПоФормуламОтДругихВидовЦен;
			Объект.Формула = Формула;
			
		КонецЕсли;
		
		Объект.ПроцентыПоДиапазонам.Очистить();
		
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ОбновлениеИнформационнойБазы

////////////////////////////////////////////////////////////////////////////////
// СТАНДАРТНЫЕ ПОДСИСТЕМЫ
#Область СтандартныеПодсистемы

// Возвращает описание блокируемых реквизитов
//
// Возвращаемое значение:
//  Массив - имена блокируемых реквизитов
//   Элемент массива - Строка в формате:
//     ИмяРеквизита[;ИмяЭлементаФормы,...]
//      где
//       ИмяРеквизита     - имя реквизита объекта
//       ИмяЭлементаФормы - имя элемента формы, связанного с реквизитом
//
Функция ПолучитьБлокируемыеРеквизитыОбъекта() Экспорт
	
	Результат = Новый Массив;
	Результат.Добавить("ВалютаЦены");
	Результат.Добавить("ЦенаВключаетНДС");
	Результат.Добавить("СпособЗаданияЦены");
	Результат.Добавить("Идентификатор");
	
	Результат.Добавить("ВариантОкругления; ВыключитьРасширенныйРежим,ВключитьРасширенныйРежим");
	Результат.Добавить("Формула");
	
	Результат.Добавить("ЦеновыеГруппы");
	Результат.Добавить("ПравилаОкругленияЦены;КоманднаяПанельПравилаОкругленияЦеныКонструкторОкругления");
	Результат.Добавить("Формулы");
	Результат.Добавить("БазовыйВидЦены");
	Результат.Добавить("Наценка");
	
	Результат.Добавить("ТочностьОкругления; ТочностьОкругления");
	Результат.Добавить("Округлять; Округлять");
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти // СтандартныеПодсистемы

#КонецЕсли