
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Элементы.ФормаКомандаЗагрузитьКлассификатор.Видимость = ПравоДоступа("Изменение", Метаданные.Справочники.ФармакоТерапевтическиеГруппы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаЗагрузитьКлассификатор(Команда)
	
	Классификаторы = Новый Массив();
	Классификаторы.Добавить("ФармакоТерапевтическиеГруппы");
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Классификаторы", Классификаторы);
	
	ОткрытьФорму("Обработка.ЗагрузкаДанныхСИТСМедицина.Форма.ЗагрузкаКлассификаторовРЛС", ПараметрыФормы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы
