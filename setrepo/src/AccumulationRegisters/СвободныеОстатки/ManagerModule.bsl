#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Процедура сообщает пользователю об ошибках проведения по регистру ТоварыНаСкладах
//
// Параметры
//	Объект - проводимый документ
//	Отказ - признак отказа от проведения документа
//	РезультатЗапроса - информация об ошибках проведения по регистру
//
Процедура СообщитьОбОшибкахПроведения(Объект, Отказ, РезультатЗапроса) Экспорт
	
	ШаблонСообщенияВНаличииМеньшеНуля = НСтр("ru = 'Номенклатура: %Номенклатура%
	|Недостаточно товара в наличии на складе %Склад%%ИсточникФинансирования% на %Количество%'");
	
	ШаблонСообщенияВНаличииМеньшеРезерваСоСклада = НСтр("ru = 'Номенклатура: %Номенклатура%
	|Превышен доступный остаток товара на складе %Склад%%ИсточникФинансирования% на %Количество%'");
	
	Выборка = РезультатЗапроса.Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Если Выборка.ВНаличии < 0 Тогда
			ШаблонСообщения = ШаблонСообщенияВНаличииМеньшеНуля;
			Количество = - Выборка.ВНаличии;
		ИначеЕсли Выборка.ВНаличии - Выборка.ВРезервеСоСклада < 0 Тогда
			ШаблонСообщения = ШаблонСообщенияВНаличииМеньшеРезерваСоСклада;
			Количество = - (Выборка.ВНаличии - Выборка.ВРезервеСоСклада);
		Иначе
			ВызватьИсключение НСтр("ru = 'Неизвестная ошибка контроля свободных остатков.'");
		КонецЕсли;
		
		Количество = НоменклатураСервер.ПересчитатьКоличествоВЕдиницахИзмерения(Выборка.Номенклатура, Количество, Выборка.Упаковка);
		
		ТекстСообщения = СтрЗаменить(ШаблонСообщения, "%Номенклатура%",
			ОбщегоНазначенияБольничнаяАптека.ПолучитьПредставлениеНоменклатуры(Выборка.Номенклатура, Выборка.СерияНоменклатуры, Выборка.Партия));
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Количество%", Строка(Количество) + " (" + Строка(Выборка.Упаковка) + ")");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Склад%", Строка(Выборка.Склад) + ?(ЗначениеЗаполнено(Выборка.МестоХранения), ", " + Выборка.МестоХранения, ""));
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ИсточникФинансирования%", ?(ЗначениеЗаполнено(Выборка.ИсточникФинансирования), " " + НСтр("ru = 'по источнику финансирования'") + " " + Строка(Выборка.ИсточникФинансирования), ""));
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, Объект,,, Отказ);
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ПрограммныйИнтерфейс

#КонецЕсли