
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Устанавливает текст заголовка списка электронных подписей.
//
// Параметры:
//  Форма - УправляемаяФорма - форма предмета электронной подписи.
//
Процедура УстановитьЗаголовокСпискаПодписей(Форма) Экспорт
	
	КоличествоПодписей = ТаблицаПодписей(Форма).Количество();
	Если КоличествоПодписей = 0 Тогда
		ТекстЗаголовка = НСтр("ru = 'Электронные подписи'");
	Иначе
		ТекстЗаголовка = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Электронные подписи (%1)'"), Формат(КоличествоПодписей, "ЧГ="));
	КонецЕсли;
	
	ЭлементТаблицаПодписей(Форма).Родитель.Родитель.Заголовок = ТекстЗаголовка;
	
КонецПроцедуры

// Устанавливает доступность команд списка электронных подписей.
//
// Параметры:
//  Форма - УправляемаяФорма - форма предмета электронной подписи.
//
Процедура УстановитьДоступностьКомандСпискаПодписей(Форма, ТекущиеДанные = Неопределено) Экспорт
	
	ЕстьПодписи = (ТаблицаПодписей(Форма).ПолучитьЭлементы().Количество() <> 0);
	ЭтоПодпись = ЕстьПодписи;
	Если ТекущиеДанные <> Неопределено Тогда
		ЭтоПодпись = ЗначениеЗаполнено(ТекущиеДанные.Объект);
	КонецЕсли;
	
	Префикс = Префикс();
	
	КомандыРаботыСоВсемиПодписями = Новый Массив;
	КомандыРаботыСоВсемиПодписями.Добавить(Префикс + "ЭлектронныеПодписиПроверитьВсе");
	КомандыРаботыСоВсемиПодписями.Добавить(Префикс + "ЭлектронныеПодписиКонтекстноеМенюПроверитьВсе");
	
	КомандаРаботыСТекущейПодписью = Новый Массив;
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиОткрытьПодпись");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиКонтекстноеМенюОткрытьПодпись");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиПроверить");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиКонтекстноеМенюПроверить");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиСохранить");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиКонтекстноеМенюСохранить");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиУдалить");
	КомандаРаботыСТекущейПодписью.Добавить(Префикс + "ЭлектронныеПодписиКонтекстноеМенюУдалить");
	
	ОбщегоНазначенияБольничнаяАптекаКлиентСервер.УстановитьСвойствоЭлементовФормы(Форма.Элементы, КомандыРаботыСоВсемиПодписями, "Доступность", ЕстьПодписи);
	ОбщегоНазначенияБольничнаяАптекаКлиентСервер.УстановитьСвойствоЭлементовФормы(Форма.Элементы, КомандаРаботыСТекущейПодписью , "Доступность", ЭтоПодпись);
	
КонецПроцедуры

// Устанавливает статус электронной подписи.
//
// Параметры:
//  СтрокаПодписи - ДанныеФормыЭлементКоллекции - строка подписи в списке формы.
//  ТекстОшибки   - Строка - текст ошибки проверки подписи.
//
Процедура ЗаполнитьСтатусПодписи(СтрокаПодписи, ТекстОшибки = "") Экспорт
	
	Если Не ЗначениеЗаполнено(СтрокаПодписи.ДатаПроверкиПодписи) Тогда
		СтрокаПодписи.Статус = "";
		Возврат;
	КонецЕсли;
	
	Если СтрокаПодписи.ПодписьВерна Тогда
		СтрокаПодписи.Статус = НСтр("ru = 'Верна'");
		
	ИначеЕсли ЗначениеЗаполнено(ТекстОшибки) Тогда
		СтрокаПодписи.Статус = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Неверна. %1'"), ТекстОшибки);
		Если Прав(СтрокаПодписи.Статус, 1) = "." Тогда
			СтрокаПодписи.Статус = Лев(СтрокаПодписи.Статус, СтрДлина(СтрокаПодписи.Статус)-1);
		КонецЕсли;
	Иначе
		СтрокаПодписи.Статус = НСтр("ru = 'Неверна'");
	КонецЕсли;
	
КонецПроцедуры

// Формирует строку общего статуса проверки подписи
//
Функция ПолучитьОбщийСтатусПроверкиПодписи(Знач ПодписьВерна, Знач СертификатДействителен, Знач ДатаПроверкиПодписи) Экспорт
	
	ДатаПроверкиПодписи = Формат(ДатаПроверкиПодписи, "ДЛФ=D") + " " + Формат(ДатаПроверкиПодписи, "ДФ='HH:mm'");
	
	Если Не ЗначениеЗаполнено(ДатаПроверкиПодписи) Тогда
		Возврат НСтр("ru = 'Не проверена'");
	КонецЕсли;
	
	Если Не ПодписьВерна Тогда
		Возврат СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Недействительна (%1)'"), ДатаПроверкиПодписи);
	КонецЕсли;
	
	Если Не СертификатДействителен Тогда
		Возврат СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Действительна, но нет доверия к сертификату (%1)'"),
			ДатаПроверкиПодписи);
	КонецЕсли;
	
	Возврат СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Действительна (%1)'"), ДатаПроверкиПодписи);
	
КонецФункции

// Формирует строку статуса проверки подписи
//
Функция ПолучитьСтатусПроверкиПодписи(Знач ДатаПроверкиПодписи, Знач ПодписьВерна, Знач ТекстОшибкиПроверкиПодписи) Экспорт
	
	Если Не ЗначениеЗаполнено(ДатаПроверкиПодписи) Тогда
		Возврат НСтр("ru = 'Не проверена'");
	КонецЕсли;
	
	Если ПодписьВерна Тогда
		Возврат НСтр("ru = 'Действительна'");
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ТекстОшибкиПроверкиПодписи) Тогда
		Возврат ТекстОшибкиПроверкиПодписи;
	КонецЕсли;
	
	Возврат НСтр("ru = 'Недействительна'");
	
КонецФункции

Функция ЭтоФайл(Объект) Экспорт
	
	ТипОбъекта = ТипЗнч(Объект);
#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
	Возврат ЭлектроннаяПодписьБольничнаяАптекаВызовСервера.ЭтоФайл(ТипОбъекта);
#Иначе
	Возврат ЭлектроннаяПодписьБольничнаяАптекаКлиентПовтИсп.ЭтоФайл(ТипОбъекта);
#КонецЕсли
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

Функция Префикс() Экспорт
	
	Возврат "ЭлектроннаяПодписьБА";
	
КонецФункции

Функция ТаблицаПодписей(Форма) Экспорт
	
	Возврат Форма[Префикс() + "ЭлектронныеПодписи"];
	
КонецФункции

Функция ЭлементТаблицаПодписей(Форма) Экспорт
	
	Возврат Форма.Элементы[Префикс() + "ЭлектронныеПодписи"];
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс