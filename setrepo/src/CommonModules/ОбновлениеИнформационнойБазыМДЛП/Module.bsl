////////////////////////////////////////////////////////////////////////////////
// Обновление информационной базы библиотеки интеграции с ГИСМ.
// 
/////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Получение сведений о библиотеке (или конфигурации).

// См. процедуру ОбновлениеИнформационнойБазыБСП.ПриДобавленииПодсистемы
Процедура ПриДобавленииПодсистемы(Описание) Экспорт
	
	Описание.Имя    = "БиблиотекаИнтеграцииМДЛП";
	Описание.Версия = "1.0.2.6";
	Описание.РежимВыполненияОтложенныхОбработчиков = "Параллельно";
	
	Описание.ТребуемыеПодсистемы.Добавить("СтандартныеПодсистемы");
	Описание.ТребуемыеПодсистемы.Добавить("БиблиотекаПодключаемогоОборудования");
	
КонецПроцедуры

// См. процедуру ОбновлениеИнформационнойБазыБСП.ПриДобавленииОбработчиковОбновления
Процедура ПриДобавленииОбработчиковОбновления(Обработчики) Экспорт
	
	Обработчик = Обработчики.Добавить();
	Обработчик.Процедура = "ОбновлениеИнформационнойБазыМДЛП.НачальноеЗаполнение";
	Обработчик.НачальноеЗаполнение = Истина;
	
	Обработчик = Обработчики.Добавить();
	Обработчик.Версия = "*";
	Обработчик.Процедура = "ОбновлениеИнформационнойБазыМДЛП.ЗаполнитьВерсиюСхемОбмена";
	Обработчик.МонопольныйРежим = Истина;
	
	Обработчик = Обработчики.Добавить();
	Обработчик.Версия = "1.0.1.9";
	Обработчик.РежимВыполнения = "Отложенно";
	Обработчик.Идентификатор = Новый УникальныйИдентификатор("d3fb1f22-eb10-11e7-b89b-68a3c4ce4c90");
	Обработчик.Процедура = "Документы.УведомлениеОбОтгрузкеМДЛП.ОбработатьДанныеДляПереходаНаНовуюВерсию";
	Обработчик.ПроцедураЗаполненияДанныхОбновления = "Документы.УведомлениеОбОтгрузкеМДЛП.ЗарегистрироватьДанныеКОбработкеДляПереходаНаНовуюВерсию";
	Обработчик.ОчередьОтложеннойОбработки = 1;
	Обработчик.ПроцедураПроверки = "ОбновлениеИнформационнойБазы.ДанныеОбновленыНаНовуюВерсиюПрограммы";
	Обработчик.ЧитаемыеОбъекты = "Документ.УведомлениеОбОтгрузкеМДЛП";
	Обработчик.ИзменяемыеОбъекты = "Документ.УведомлениеОбОтгрузкеМДЛП";
	Обработчик.БлокируемыеОбъекты = "Документ.УведомлениеОбОтгрузкеМДЛП";
	Обработчик.Комментарий = НСтр("ru = 'Переносит цену и сумму НДС из табличной части ""Номера упаковок"" в ""Товары"" документов ""Уведомления об отгрузке МДЛП"".'");
	
	Обработчик = Обработчики.Добавить();
	Обработчик.Версия = "1.0.1.9";
	Обработчик.РежимВыполнения = "Отложенно";
	Обработчик.Идентификатор = Новый УникальныйИдентификатор("ebbf3274-eb10-11e7-b89b-68a3c4ce4c90");
	Обработчик.Процедура = "Документы.УведомлениеОПриемкеМДЛП.ОбработатьДанныеДляПереходаНаНовуюВерсию";
	Обработчик.ПроцедураЗаполненияДанныхОбновления = "Документы.УведомлениеОПриемкеМДЛП.ЗарегистрироватьДанныеКОбработкеДляПереходаНаНовуюВерсию";
	Обработчик.ОчередьОтложеннойОбработки = 1;
	Обработчик.ПроцедураПроверки = "ОбновлениеИнформационнойБазы.ДанныеОбновленыНаНовуюВерсиюПрограммы";
	Обработчик.ЧитаемыеОбъекты = "Документ.УведомлениеОПриемкеМДЛП";
	Обработчик.ИзменяемыеОбъекты = "Документ.УведомлениеОПриемкеМДЛП";
	Обработчик.БлокируемыеОбъекты = "Документ.УведомлениеОПриемкеМДЛП";
	Обработчик.Комментарий = НСтр("ru = 'Переносит цену и сумму НДС из табличной части ""Номера упаковок"" в ""Товары"" документов ""Уведомления о приемке МДЛП"".'");
	
	Обработчик = Обработчики.Добавить();
	Обработчик.Версия = "1.0.1.10";
	Обработчик.РежимВыполнения = "Отложенно";
	Обработчик.Идентификатор = Новый УникальныйИдентификатор("ec81c779-19d9-4d20-86fd-66cb950ada2b");
	Обработчик.Процедура = "Справочники.МДЛППрисоединенныеФайлы.ОбработатьДанныеДляПереходаНаНовуюВерсию";
	Обработчик.ПроцедураЗаполненияДанныхОбновления = "Справочники.МДЛППрисоединенныеФайлы.ЗарегистрироватьДанныеКОбработкеДляПереходаНаНовуюВерсию";
	Обработчик.ОчередьОтложеннойОбработки = 1;
	Обработчик.ПроцедураПроверки = "ОбновлениеИнформационнойБазы.ДанныеОбновленыНаНовуюВерсиюПрограммы";
	Обработчик.ЧитаемыеОбъекты = "Справочник.МДЛППрисоединенныеФайлы";
	Обработчик.ИзменяемыеОбъекты = "Справочник.МДЛППрисоединенныеФайлы";
	Обработчик.БлокируемыеОбъекты = "Справочник.МДЛППрисоединенныеФайлы";
	Обработчик.Комментарий = НСтр("ru = 'Заполняет хеш сумму сообщений протокола обмена.'");
	
КонецПроцедуры

// См. процедуру ОбновлениеИнформационнойБазыБСП.ПередОбновлениемИнформационнойБазы
Процедура ПередОбновлениемИнформационнойБазы() Экспорт
	
КонецПроцедуры

// См. процедуру ОбновлениеИнформационнойБазыБСП.ПослеОбновленияИнформационнойБазы
Процедура ПослеОбновленияИнформационнойБазы(Знач ПредыдущаяВерсия, Знач ТекущаяВерсия,
		Знач ВыполненныеОбработчики, ВыводитьОписаниеОбновлений, МонопольныйРежим) Экспорт
	
КонецПроцедуры

// См. процедуру ОбновлениеИнформационнойБазыБСП.ПриПодготовкеМакетаОписанияОбновлений
Процедура ПриПодготовкеМакетаОписанияОбновлений(Знач Макет) Экспорт
	

КонецПроцедуры

// Позволяет переопределить режим обновления данных информационной базы.
// Для использования в редких (нештатных) случаях перехода, не предусмотренных в
// стандартной процедуре определения режима обновления.
//
// Параметры:
//   РежимОбновленияДанных - Строка - в обработчике можно присвоить одно из значений:
//              "НачальноеЗаполнение"     - если это первый запуск пустой базы (области данных);
//              "ОбновлениеВерсии"        - если выполняется первый запуск после обновление конфигурации базы данных;
//              "ПереходСДругойПрограммы" - если выполняется первый запуск после обновление конфигурации базы данных, 
//                                          в которой изменилось имя основной конфигурации.
//
//   СтандартнаяОбработка  - Булево - если присвоить Ложь, то стандартная процедура
//                                    определения режима обновления не выполняется, 
//                                    а используется значение РежимОбновленияДанных.
//
Процедура ПриОпределенииРежимаОбновленияДанных(РежимОбновленияДанных, СтандартнаяОбработка) Экспорт
 
КонецПроцедуры

// Добавляет в список процедуры-обработчики перехода с другой программы (с другим именем конфигурации).
// Например, для перехода между разными, но родственными конфигурациями: базовая -> проф -> корп.
// Вызывается перед началом обновления данных ИБ.
//
// Параметры:
//  Обработчики - ТаблицаЗначений - с колонками:
//    * ПредыдущееИмяКонфигурации - Строка - имя конфигурации, с которой выполняется переход;
//                                           или "*", если нужно выполнять при переходе с любой конфигурации.
//    * Процедура                 - Строка - полное имя процедуры-обработчика перехода с программы ПредыдущееИмяКонфигурации. 
//                                  Например, "ОбновлениеИнформационнойБазыУПП.ЗаполнитьУчетнуюПолитику"
//                                  Обязательно должна быть экспортной.
//
// Пример добавления процедуры-обработчика в список:
//  Обработчик = Обработчики.Добавить();
//  Обработчик.ПредыдущееИмяКонфигурации  = "УправлениеТорговлей";
//  Обработчик.Процедура                  = "ОбновлениеИнформационнойБазыУПП.ЗаполнитьУчетнуюПолитику";
//
Процедура ПриДобавленииОбработчиковПереходаСДругойПрограммы(Обработчики) Экспорт
 
КонецПроцедуры

// Вызывается после выполнения всех процедур-обработчиков перехода с другой программы (с другим именем конфигурации),
// и до начала выполнения обновления данных ИБ.
//
// Параметры:
//  ПредыдущееИмяКонфигурации    - Строка - имя конфигурации до перехода.
//  ПредыдущаяВерсияКонфигурации - Строка - имя предыдущей конфигурации (до перехода).
//  Параметры                    - Структура - 
//    * ВыполнитьОбновлениеСВерсии   - Булево - по умолчанию Истина. Если установить Ложь, 
//        то будут выполнена только обязательные обработчики обновления (с версией "*").
//    * ВерсияКонфигурации           - Строка - номер версии после перехода. 
//        По умолчанию, равен значению версии конфигурации в свойствах метаданных.
//        Для того чтобы выполнить, например, все обработчики обновления с версии ПредыдущаяВерсияКонфигурации, 
//        следует установить значение параметра в ПредыдущаяВерсияКонфигурации.
//        Для того чтобы выполнить вообще все обработчики обновления, установить значение "0.0.0.1".
//    * ОчиститьСведенияОПредыдущейКонфигурации - Булево - по умолчанию Истина. 
//        Для случаев когда предыдущая конфигурация совпадает по имени с подсистемой текущей конфигурации, следует указать Ложь.
//
Процедура ПриЗавершенииПереходаСДругойПрограммы(Знач ПредыдущееИмяКонфигурации, Знач ПредыдущаяВерсияКонфигурации, Параметры) Экспорт
 
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПервоначальноеЗаполнениеИОбновлениеИнформационнойБазы

Процедура НачальноеЗаполнение() Экспорт
	
	
	
КонецПроцедуры

#КонецОбласти

Процедура ЗаполнитьВерсиюСхемОбмена(Параметры) Экспорт
	
	Версия = Константы.ВерсияСхемОбменаМДЛП.Получить();
	ДоступныеВерсии = ИнтеграцияМДЛП.ДоступныеВерсииСхемОбмена();
	ВерсияПоУмолчанию = ИнтеграцияМДЛП.ВерсияСхемОбменаПоУмолчанию();
	Если ДоступныеВерсии.Найти(Версия) = Неопределено
	 Или ОбщегоНазначенияКлиентСервер.СравнитьВерсииБезНомераСборки(ВерсияПоУмолчанию + ".0", Версия + ".0") > 0 Тогда
		Если Не Параметры.МонопольныйРежим Тогда
			Параметры.МонопольныйРежим = Истина;
			Возврат;
		КонецЕсли;
		Константы.ВерсияСхемОбменаМДЛП.Установить(ВерсияПоУмолчанию);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
