
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Возвращает список ссылок на ИТС:Медицина и НомеровРЛС
//
// Параметры
//  Товары - Массив(СправочникСсылка.РегистрЛекарственныхСредств) - аптечные товары, для которых нужно вернуть ссылки
//
// Возвращаемое значение
//  Массив(Структура("Ссылка, СсылкаНаОписание, НомерРЛС"))
//
Функция ПолучитьСсылкиНаОписанияТоваров(Товары) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Ссылка КАК Ссылка,
	|	СсылкаНаОписаниеВБазеРЛС КАК СсылкаНаОписание,
	|	НомерРЛС КАК НомерРЛС
	|ИЗ
	|	Справочник.РегистрЛекарственныхСредств КАК РегистрЛекарственныхСредств
	|ГДЕ
	|	(НЕ РегистрЛекарственныхСредств.НомерРЛС = 0)
	|	И РегистрЛекарственныхСредств.Ссылка В (&Товары)
	|");
	Запрос.УстановитьПараметр("Товары", Товары);
	
	СсылкиНаОписания = Новый Массив;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		СсылкаНаОписание = Новый Структура("Ссылка, СсылкаНаОписание, НомерРЛС");
		ЗаполнитьЗначенияСвойств(СсылкаНаОписание, Выборка);
		СсылкиНаОписания.Добавить(СсылкаНаОписание);
		
	КонецЦикла;
	
	Возврат СсылкиНаОписания;
	
КонецФункции

// Возвращает настройки взаимодействия с диском ИТС:Медицина
//
// Возвращаемое значение
//  Структура
//
Функция ПолучитьНастройкиИТСМедицина() Экспорт
	
	Настройки = Новый Структура;
	Настройки.Вставить("ПериодичностьПроверкиКаталогаОбмена", ПериодичностьПроверкиКаталогаОбмена());
	Настройки.Вставить("ПутьЗапускаИТСвТерминальномРежиме", Константы.ПутьЗапускаИТСвТерминальномРежиме.Получить());
	Настройки.Вставить("ИсточникИнформацииРегистраЛекарственныхСредств", Константы.ИсточникИнформацииРегистраЛекарственныхСредств.Получить());
	
	Возврат Настройки;
	
КонецФункции

// Загружает классификатор в справочник из файла
//
// Параметры
//  Справочник - структура - содержит поля "Номер", "Наименование", "ИмяМетаданных"
//
Процедура ЗагрузитьКлассификаторИзФайла(Справочник, ИмяФайла, ФорматДанных = "xml") Экспорт
	
	Если ФорматДанных = "xml" Тогда
		
		ДанныеКлассификатора = РаботаСРЛС.РазобратьСодержимоеФайлаСДискаИТС(ИмяФайла);
		
	ИначеЕсли ФорматДанных = "json" Тогда
		
		ДанныеКлассификатора = РаботаСРЛС.ПрочитатьДанныеКлассификатораJSON(ИмяФайла);
		
	КонецЕсли;
	
	Если ДанныеКлассификатора.ТипСообщения <> "Классификатор" Или ДанныеКлассификатора.Идентификатор <> Справочник.Номер Тогда
		ТекстСообщения = НСтр("ru='Ошибка загрузки классификатора %1: неверный файл классификатора.'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%1", Справочник.Наименование);
		ВызватьИсключение ТекстСообщения;
	КонецЕсли;
	
	РаботаСРЛС.ЗагрузитьКлассификатор(ДанныеКлассификатора.Элементы, Метаданные.Справочники.Найти(Справочник.ИмяМетаданных));
	
КонецПроцедуры

// Возвращает номер справочника РЛС на диске ИТС по наименованию
//
// Параметры
//  ИмяСправочника - Строка - имя справочника для которого нужно получить номер
//
// Возвращаемое значение
//  Число - номер справочника
//
Функция ПолучитьНомерСправочникаНаИТС(ИмяСправочника) Экспорт
	Перем НомерСправочника;
	
	СправочникиРЛС = ИТСМедицинаКлиентСервер.ПолучитьСтруктуруСправочниковНаИТС();
	
	Возврат ?(СправочникиРЛС.Свойство(ИмяСправочника, НомерСправочника), НомерСправочника.Номер, "0");
	
КонецФункции

// Возвращает Соответствие справочников конфигурации классификаторам РЛС
//
// Возвращаемое значение
//  Соответствие - Ключ - Имя справочника как оно задано в конфигураторе
//                 Значение - Структура с полями Номер и Наименование классификатора РЛС
//
Функция ПолучитьСоответствиеСправочниковКонфигурацииКлассификаторамРЛС() Экспорт
	
	Соответствие = Новый Соответствие;
	Соответствие.Вставить("АТХКлассификация"             , Новый Структура("Номер, Наименование", "6" , НСтр("ru = 'АТХ'")));
	Соответствие.Вставить("ТоварыФармацевтическогоРынка" , Новый Структура("Номер, Наименование", "20", НСтр("ru = 'Товары фармацевтического рынка'")));
	Соответствие.Вставить("ФармакологическиеГруппы"      , Новый Структура("Номер, Наименование", "4" , НСтр("ru = 'Фармакологическая группа'")));
	
	Возврат Соответствие;
	
КонецФункции

// Возвращает параметры поиска синонимов товара
//
// Параметры
//  Товар - СправочникСсылка.РегистрЛекарственныхСредств
//
// Возвращаемое значение
//  Структура
//
Функция ПолучитьПараметрыПоискаСинонимовТовара(Товар) Экспорт
	
	ЗапрашиваемыеРеквизиты = Новый Структура;
	ЗапрашиваемыеРеквизиты.Вставить("ТорговоеНаименование", "ТорговоеНаименование.Наименование");
	ЗапрашиваемыеРеквизиты.Вставить("ТорговоеНаименованиеДействующиеВеществаМНН", "ЕстьNULL(ТорговоеНаименование.ДействующиеВеществаМНН.НаименованиеПолное, """")");
	ЗапрашиваемыеРеквизиты.Вставить("ДействующиеВеществаМНН", "ДействующиеВеществаМНН.НаименованиеПолное");
	ЗапрашиваемыеРеквизиты.Вставить("Наименование");
	
	Реквизиты =  ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Товар, ЗапрашиваемыеРеквизиты);
	
	ПараметрыПоиска = Новый Структура;
	Если ЗначениеЗаполнено(Реквизиты.ТорговоеНаименование) Тогда
		ТорговыеНаименования = Новый Массив;
		ТорговыеНаименования.Добавить(Реквизиты.ТорговоеНаименование);
		ПараметрыПоиска.Вставить("ТорговыеНаименования", Новый Структура("Включить", ТорговыеНаименования));
		ДействующиеВеществаМНН = Новый Массив;
		ДействующиеВеществаМНН.Добавить(Реквизиты.ТорговоеНаименованиеДействующиеВеществаМНН);
		ПараметрыПоиска.Вставить("ДействующиеВеществаМНН", Новый Структура("Включить", ДействующиеВеществаМНН));
	ИначеЕсли ЗначениеЗаполнено(Реквизиты.ДействующиеВеществаМНН) Тогда
		ДействующиеВеществаМНН = Новый Массив;
		ДействующиеВеществаМНН.Добавить(Реквизиты.ДействующиеВеществаМНН);
		ПараметрыПоиска.Вставить("ДействующиеВеществаМНН", Новый Структура("Включить", ДействующиеВеществаМНН));
		ПараметрыПоиска.Вставить("Текст", Новый Структура("Включить", Реквизиты.ДействующиеВеществаМНН));
	Иначе
		ПараметрыПоиска.Вставить("Текст", Новый Структура("Включить", Реквизиты.Наименование));
	КонецЕсли;
	
	Возврат ПараметрыПоиска;
	
КонецФункции

// Сохраняет параметры аутентификации на сайте 1С:ИТС
//
Процедура СохранитьПараметрыАутентификации(Знач Логин, Знач Пароль) Экспорт
	
	Владелец = ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Обработки.ЗагрузкаДанныхСИТСМедицина.ПолноеИмя());
	УстановитьПривилегированныйРежим(Истина);
	ОбщегоНазначения.ЗаписатьДанныеВБезопасноеХранилище(Владелец, Пароль);
	ОбщегоНазначения.ЗаписатьДанныеВБезопасноеХранилище(Владелец, Логин, "Логин");
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

// Возвращает сохраненные параметры аутентификации на сайте 1С:ИТС
//
Функция ПолучитьПараметрыАутентификации(Логин, Пароль) Экспорт
	
	Владелец = ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Обработки.ЗагрузкаДанныхСИТСМедицина.ПолноеИмя());
	УстановитьПривилегированныйРежим(Истина);
	Результат = ОбщегоНазначения.ПрочитатьДанныеИзБезопасногоХранилища(Владелец, "Логин, Пароль");
	УстановитьПривилегированныйРежим(Ложь);
	
	Логин = Результат.Логин;
	Пароль = Результат.Пароль;
	
	Возврат Не ПустаяСтрока(Логин) И Не ПустаяСтрока(Пароль);
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

Функция ПериодичностьПроверкиКаталогаОбмена()
	
	ПериодичностьПроверки = Константы.ПериодичностьПроверкиКаталогаОбменаСДискомИТС.Получить();
	Если ПериодичностьПроверки < 5 Тогда
		ПериодичностьПроверки = 5;
		УстановитьПривилегированныйРежим(Истина);
		Константы.ПериодичностьПроверкиКаталогаОбменаСДискомИТС.Установить(ПериодичностьПроверки);
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	
	Возврат ПериодичностьПроверки;
	
КонецФункции

// Заполняет перечень запросов внешних разрешений, которые обязательно должны быть предоставлены
// при создании информационной базы или обновлении программы.
//
// Параметры:
//  ЗапросыРазрешений - Массив - список значений, возвращенных функцией.
//                      РаботаВБезопасномРежиме.ЗапросНаИспользованиеВнешнихРесурсов().
//
Процедура ПриЗаполненииРазрешенийНаДоступКВнешнимРесурсам(ЗапросыРазрешений) Экспорт
	
	Разрешения = Новый Массив;
	
	Протокол = "HTTP";
	Адрес = "its.1c.ru";
	Порт = Неопределено;
	Описание = НСтр("ru = 'Загрузка медицинских классификаторов из интернета.'");
	Разрешения.Добавить(
		РаботаВБезопасномРежиме.РазрешениеНаИспользованиеИнтернетРесурса(Протокол, Адрес, Порт, Описание));
	
	Владелец = ОбщегоНазначения.ИдентификаторОбъектаМетаданных(Метаданные.Справочники.РегистрЛекарственныхСредств);
	РаботаВБезопасномРежиме.ЗапросНаИспользованиеВнешнихРесурсов(Разрешения, Владелец);
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
