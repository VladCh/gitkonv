
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ
#Область ОбработчикиСобытий

Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	ВыбраннаяОперация = Неопределено;
	
	Если Параметры.Свойство("Ключ") И ЗначениеЗаполнено(Параметры.Ключ) Тогда
		ВыбраннаяОперация = Параметры.Ключ.ТипОперации;
	ИначеЕсли Параметры.Свойство("ЗначенияЗаполнения") Тогда
		Параметры.ЗначенияЗаполнения.Свойство("ТипОперации", ВыбраннаяОперация);
	ИначеЕсли Параметры.Свойство("ЗначениеКопирования") Тогда
		Запрос = Новый Запрос("ВЫБРАТЬ ТипОперации ИЗ Документ.ВводОстатков ГДЕ Ссылка = &Ссылка");
		Запрос.УстановитьПараметр("Ссылка", Параметры.ЗначениеКопирования);
		Выборка = Запрос.Выполнить().Выбрать();
		Выборка.Следующий();
		ВыбраннаяОперация = Выборка.ТипОперации;
	ИначеЕсли Параметры.Свойство("ОтборПоТипамОпераций") И Параметры.ОтборПоТипамОпераций.Количество() = 1 Тогда
		ВыбраннаяОперация = Параметры.ОтборПоТипамОпераций[0].Значение;
		ЗначенияЗаполнения = Новый Структура;
		ЗначенияЗаполнения.Вставить("ТипОперации", ВыбраннаяОперация);
		Параметры.Вставить("ЗначенияЗаполнения", ЗначенияЗаполнения);
		Если Параметры.Свойство("Организация") Тогда
			ЗначенияЗаполнения.Вставить("Организация", Параметры.Организация);
		КонецЕсли;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ВыбраннаяОперация) Тогда
		СтандартнаяОбработка = Ложь;
		СоответствиеТиповОперацийФормам = Новый Соответствие;
		СоответствиеТиповОперацийФормам.Вставить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиВАвтономныхКассахККМПоРозничнойВыручке, "ФормаКассы");
		СоответствиеТиповОперацийФормам.Вставить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров, "ФормаТовары");
		СоответствиеТиповОперацийФормам.Вставить(Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях, "ФормаТовары");
		ВыбраннаяФорма = СоответствиеТиповОперацийФормам[ВыбраннаяОперация];
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытий

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Имена реквизитов, от значений которых зависят параметры учета номенклатуры
//
// Возвращаемое значение:
//   Строка - имена реквизитов, перечисленные через запятую
//
Функция ИменаРеквизитовДляЗаполненияПараметровУчетаНоменклатуры() Экспорт
	
	Возврат "Склад";
	
КонецФункции

// Возвращает параметры учета для номенклатуры, указанной в документе
//
// Параметры
//   Объект - Структура - структура значений реквизитов объекта, необходимых для заполнения параметров указания серий
// Возвращаемое значение
//   Структура - Состав полей задается в функции НоменклатураКлиентСервер.ПараметрыУчетаНоменклатуры
//
Функция ПараметрыУчетаНоменклатуры(Объект) Экспорт
	
	ПараметрыУчета = ЗапасыСервер.ПараметрыУчетаНоменклатуры();
	ПараметрыУчета.ПолноеИмяОбъекта = ПустаяСсылка().Метаданные().ПолноеИмя();
	
	ПараметрыУчетаНаСкладе = СкладыСервер.ПараметрыУчетаНоменклатуры(Объект.Склад);
	ПараметрыУчета.ИспользоватьСерии = ПараметрыУчетаНаСкладе.ИспользоватьСерииНоменклатуры;
	ПараметрыУчета.ИспользоватьПартии = ПараметрыУчетаНаСкладе.ИспользоватьПартии;
	ПараметрыУчета.Склад = Объект.Склад;
	
	Возврат ПараметрыУчета;
	
КонецФункции

// Возвращает текст запроса для расчета статусов указания параметров учета номенклатуры
//
// Параметры
//   ПараметрыУчетаНоменклатуры - Структура - состав полей задается в функции ЗапасыСервер.ПараметрыУчетаНоменклатуры
//
// Возвращаемое значение
//   Строка - текст запроса
//
Функция ТекстЗапросаРасчетаСтатусовУчетаНоменклатуры(ПараметрыУчетаНоменклатуры) Экспорт
	
	Возврат ЗапасыСервер.ТекстЗапросаРасчетаСтатусовУчетаНоменклатуры(ПараметрыУчетаНоменклатуры);
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// Проведение
#Область Проведение

// Формирует таблицы значений, содержащие данные для проведения документа.
//
Процедура СформироватьТаблицыДвиженийДляПроведения(ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	ОсновныеДанныеДокумента = ПодготовитьОсновныеДанныеДляПроведения(ДополнительныеСвойства);
	
	Если ОсновныеДанныеДокумента.ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров
	 Или ОсновныеДанныеДокумента.ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях Тогда
		ИнициализироватьКлючиАналитикиВидаУчета(ОсновныеДанныеДокумента);
		ИнициализироватьКлючиАналитикиУчетаНоменклатуры(ОсновныеДанныеДокумента);
	КонецЕсли;
	
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаВтТаблицаТовары());
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(
		ДополнительныеСвойства,
		ТекстЗапросаТоварыНаСкладах(),
		?(ОсновныеДанныеДокумента.УчетВОтделениях, Метаданные.РегистрыНакопления.ТоварыНаСкладахВОтделениях, Метаданные.РегистрыНакопления.ТоварыНаСкладах));
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаСвободныеОстатки(), Метаданные.РегистрыНакопления.СвободныеОстатки);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаСебестоимостьТоваров(), Метаданные.РегистрыНакопления.СебестоимостьТоваров);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаДанныеПоступленийДляПроектаПоМониторингуЦен(), Метаданные.РегистрыНакопления.ДанныеПоступленийДляПроектаПоМониторингуЦен);
	ПроведениеБольничнаяАптека.ДобавитьТекстЗапросаДвижений(ДополнительныеСвойства, ТекстЗапросаДенежныеСредстваВКассахККМ(), Метаданные.РегистрыНакопления.ДенежныеСредстваВКассахККМ);
	
	Запрос = Новый Запрос(ПроведениеБольничнаяАптека.ПолучитьТекстЗапросаДвижений(ДополнительныеСвойства, Регистры));
	
	Для Каждого ДанныеДокумента Из ОсновныеДанныеДокумента Цикл
		Запрос.УстановитьПараметр(ДанныеДокумента.Ключ, ДанныеДокумента.Значение);
	КонецЦикла;
	
	ПроведениеБольничнаяАптека.ЗаполнитьТаблицыДвижений(ДополнительныеСвойства, Запрос.ВыполнитьПакет(), Регистры);
	
КонецПроцедуры

Функция ПодготовитьОсновныеДанныеДляПроведения(ДополнительныеСвойства)
	
	ЗапрашиваемыеДанные = Новый Структура;
	ЗапрашиваемыеДанные.Вставить("Ссылка");
	ЗапрашиваемыеДанные.Вставить("Период", "Дата");
	ЗапрашиваемыеДанные.Вставить("ТипОперации");
	ЗапрашиваемыеДанные.Вставить("Организация");
	ЗапрашиваемыеДанные.Вставить("Склад");
	ЗапрашиваемыеДанные.Вставить("Контрагент");
	ЗапрашиваемыеДанные.Вставить("ИсточникФинансирования");
	ЗапрашиваемыеДанные.Вставить("ПринятьНДСКВычету");
	
	ОсновныеДанныеДокумента = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(
		ПроведениеБольничнаяАптека.ПолучитьСсылкуНаДокументДляПроведения(ДополнительныеСвойства),
		ЗапрашиваемыеДанные);
	
	ОсновныеДанныеДокумента.Вставить("ВестиУчетПоИсточникамФинансирования", ПолучитьФункциональнуюОпцию("ИспользоватьИсточникиФинансирования"));
	ОсновныеДанныеДокумента.Вставить("УчетВОтделениях", ОсновныеДанныеДокумента.ТипОперации = Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях);
	ОсновныеДанныеДокумента.Вставить("ХозяйственнаяОперация", Перечисления.ХозяйственныеОперации.ОприходованиеТоваров);
	
	Если Не ОсновныеДанныеДокумента.ВестиУчетПоИсточникамФинансирования Тогда
		ОсновныеДанныеДокумента.ИсточникФинансирования = Справочники.ИсточникиФинансирования.ПустаяСсылка();
	КонецЕсли;
	
	ЗапасыСервер.ПриПодготовкеОсновныхДанныхДляПроведения(ДополнительныеСвойства, ОсновныеДанныеДокумента);
	
	Возврат ОсновныеДанныеДокумента;
	
КонецФункции

Функция ТекстЗапросаВтТаблицаТовары()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки                               КАК НомерСтроки,
	|	&Организация                                            КАК Организация,
	|	&Склад                                                  КАК Склад,
	|	ЗНАЧЕНИЕ(Справочник.МестаХранения.ПустаяСсылка)         КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура                              КАК Номенклатура,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (&СтатусУчетПоСериям, &СтатусУчетСебестоимостиПоСериям)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                   КАК СерияНоменклатуры,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий В (&СтатусУчетПоПартиям, &СтатусУчетСебестоимостиПоПартиям)
	|			ТОГДА ТаблицаТовары.Партия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                   КАК Партия,
	|	&ИсточникФинансирования                                 КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                                КАК Количество,
	|	ТаблицаТовары.ЦенаПроизводителя                         КАК ЦенаПроизводителя,
	|	ТаблицаТовары.ЕдиницаИзмерения                          КАК ЕдиницаИзмерения,
	|	ТаблицаТовары.Сумма                                     КАК Сумма,
	|	ТаблицаТовары.СтавкаНДС                                 КАК СтавкаНДС,
	|	ТаблицаТовары.СуммаНДС                                  КАК СуммаНДС,
	|	ТаблицаТовары.СуммаСНДС                                 КАК СуммаСНДС,
	|	ТаблицаТовары.СуммаСНДС - ТаблицаТовары.СуммаНДС        КАК СуммаБезНДС,
	|	ТаблицаТовары.СуммаРегл                                 КАК СуммаРегл,
	|	ТаблицаТовары.НДСРегл                                   КАК НДСРегл,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (&СтатусУчетСебестоимостиПоСериям)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                   КАК СерияНоменклатурыДляСебестоимости,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий В (&СтатусУчетСебестоимостиПоПартиям)
	|			ТОГДА ТаблицаТовары.Партия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                                   КАК ПартияДляСебестоимости
	|ПОМЕСТИТЬ ВтТаблицаТовары
	|ИЗ
	|	Документ.ВводОстатков.Товары КАК ТаблицаТовары
	|
	|ГДЕ
	|	&ТипОперации В (ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров),
	|					ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях))
	|	И ТаблицаТовары.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТоварыНаСкладах()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки               КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)  КАК ВидДвижения,
	|	&Период                                 КАК Период,
	|	ТаблицаТовары.Организация               КАК Организация,
	|	ТаблицаТовары.Склад                     КАК Склад,
	|	ТаблицаТовары.МестоХранения             КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура              КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатуры         КАК СерияНоменклатуры,
	|	ТаблицаТовары.Партия                    КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования    КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                КАК Количество
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	&ТипОперации В (ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров),
	|					ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях))
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаСвободныеОстатки()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки               КАК НомерСтроки,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)  КАК ВидДвижения,
	|	&Период                                 КАК Период,
	|	ТаблицаТовары.Организация               КАК Организация,
	|	ТаблицаТовары.Склад                     КАК Склад,
	|	ТаблицаТовары.МестоХранения             КАК МестоХранения,
	|	ТаблицаТовары.Номенклатура              КАК Номенклатура,
	|	ТаблицаТовары.СерияНоменклатуры         КАК СерияНоменклатуры,
	|	ТаблицаТовары.Партия                    КАК Партия,
	|	ТаблицаТовары.ИсточникФинансирования    КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                КАК ВНаличии
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	&ТипОперации В (ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров),
	|					ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях))
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаСебестоимостьТоваров()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки                   КАК НомерСтроки,
	|	&Период                                     КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)      КАК ВидДвижения,
	|	АналитикаУчетаНоменклатуры.КлючАналитики    КАК АналитикаУчетаНоменклатуры,
	|	АналитикаВидаУчета.КлючАналитики            КАК АналитикаВидаУчета,
	|	ВЫБОР
	|		КОГДА &ТипОперации = ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях)
	|			ТОГДА ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ТоварыВОтделениях)
	|		ИНАЧЕ ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ТоварыНаСкладах)
	|	КОНЕЦ                                       КАК РазделУчета,
	|	ТаблицаТовары.Количество                    КАК Количество,
	|	ТаблицаТовары.СуммаСНДС                     КАК Стоимость,
	|	ТаблицаТовары.СуммаБезНДС                   КАК СтоимостьБезНДС,
	|	ВЫБОР
	|		КОГДА &ПринятьНДСКВычету
	|			ТОГДА ТаблицаТовары.СуммаРегл
	|		ИНАЧЕ ТаблицаТовары.СуммаРегл + ТаблицаТовары.НДСРегл
	|	КОНЕЦ                                       КАК СтоимостьРегл,
	|	&ХозяйственнаяОперация                      КАК ХозяйственнаяОперация,
	|	НЕОПРЕДЕЛЕНО                                КАК КорАналитикаУчетаНоменклатуры,
	|	НЕОПРЕДЕЛЕНО                                КАК КорАналитикаВидаУчета,
	|	НЕОПРЕДЕЛЕНО                                КАК КорРазделУчета,
	|	0                                           КАК КорКоличество
	|ИЗ
	|	ВтТаблицаТовары КАК ТаблицаТовары
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры
	|		ПО 
	|			ТаблицаТовары.Номенклатура                        = АналитикаУчетаНоменклатуры.Номенклатура
	|			И ТаблицаТовары.СерияНоменклатурыДляСебестоимости = АналитикаУчетаНоменклатуры.СерияНоменклатуры
	|			И ТаблицаТовары.ПартияДляСебестоимости            = АналитикаУчетаНоменклатуры.Партия
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаВидаУчета КАК АналитикаВидаУчета
	|		ПО 
	|			ТаблицаТовары.Организация                     = АналитикаВидаУчета.Организация
	|			И ТаблицаТовары.Склад                         = АналитикаВидаУчета.Склад
	|			И ТаблицаТовары.ИсточникФинансирования        = АналитикаВидаУчета.ИсточникФинансирования
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаДанныеПоступленийДляПроектаПоМониторингуЦен()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки                   КАК НомерСтроки,
	|	&Период                                     КАК Период,
	|	&Организация                                КАК Организация,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий НЕ В (&СтатусПартииНеУказываются)
	|			ТОГДА ТаблицаТовары.Партия.Поставщик
	|		ИНАЧЕ &Контрагент
	|	КОНЕЦ                                       КАК Поставщик,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий НЕ В (&СтатусПартииНеУказываются)
	|			ТОГДА ТаблицаТовары.Партия.ДокументОприходования
	|		ИНАЧЕ &Ссылка
	|	КОНЕЦ                                       КАК ДокументПоступления,
	|	ТаблицаТовары.Номенклатура                  КАК Номенклатура,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий НЕ В (&СтатусСерииНеУказываются)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ                                       КАК СерияНоменклатуры,
	|	&ИсточникФинансирования                     КАК ИсточникФинансирования,
	|	ТаблицаТовары.Количество                    КАК Количество,
	|	ТаблицаТовары.СуммаСНДС                     КАК Стоимость,
	|	ТаблицаТовары.СуммаНДС                      КАК НДС,
	|	ТаблицаТовары.ЕдиницаИзмерения              КАК ЕдиницаИзмерения,
	|	ТаблицаТовары.ЦенаПроизводителя             КАК ЦенаПроизводителя
	|ИЗ
	|	Документ.ВводОстатков.Товары КАК ТаблицаТовары
	|ГДЕ
	|	&ТипОперации В (ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров),
	|					ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях))
	|	И ТаблицаТовары.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаДенежныеСредстваВКассахККМ()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	&Период	                               КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	&Организация                           КАК Организация,
	|	ДанныеДокумента.КассаККМ               КАК КассаККМ,
	|	ДанныеДокумента.Сумма                  КАК Сумма
	|	
	|ИЗ
	|	Документ.ВводОстатков.КассыККМ КАК ДанныеДокумента
	|	
	|ГДЕ
	|	&ТипОперации В (ЗНАЧЕНИЕ(Перечисление.ТипыОперацийВводаОстатков.ОстаткиВАвтономныхКассахККМПоРозничнойВыручке))
	|	И ДанныеДокумента.Ссылка = &Ссылка
	|	
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции

Процедура ИнициализироватьКлючиАналитикиУчетаНоменклатуры(Реквизиты)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ТаблицаТовары.Номенклатура КАК Номенклатура,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияСерий В (&СтатусУчетСебестоимостиПоСериям)
	|			ТОГДА ТаблицаТовары.СерияНоменклатуры
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.СерииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ КАК СерияНоменклатуры,
	|	ВЫБОР
	|		КОГДА ТаблицаТовары.СтатусУказанияПартий В (&СтатусУчетСебестоимостиПоПартиям)
	|			ТОГДА ТаблицаТовары.Партия
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|	КОНЕЦ КАК Партия
	|ПОМЕСТИТЬ втТаблицаАналитики
	|ИЗ
	|	Документ.ВводОстатков.Товары КАК ТаблицаТовары
	|ГДЕ
	|	ТаблицаТовары.Ссылка = &Ссылка
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Номенклатура,
	|	СерияНоменклатуры,
	|	Партия";
	
	Запрос.УстановитьПараметр("Ссылка", Реквизиты.Ссылка);
	Запрос.УстановитьПараметр("Контрагент", Реквизиты.Контрагент);
	Запрос.УстановитьПараметр("УчетВОтделениях", Реквизиты.УчетВОтделениях);
	Запрос.УстановитьПараметр("СтатусУчетСебестоимостиПоСериям", Реквизиты.СтатусУчетСебестоимостиПоСериям);
	Запрос.УстановитьПараметр("СтатусУчетСебестоимостиПоПартиям", Реквизиты.СтатусУчетСебестоимостиПоПартиям);
	Запрос.Выполнить();
	
	Справочники.КлючиАналитикиУчетаНоменклатуры.ИнициализироватьКлючиАналитики(Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

Процедура ИнициализироватьКлючиАналитикиВидаУчета(Реквизиты)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	&Организация КАК Организация,
	|	&Склад КАК Склад,
	|	&ИсточникФинансирования КАК ИсточникФинансирования
	|ПОМЕСТИТЬ втТаблицаАналитики
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	Организация,
	|	Склад,
	|	ИсточникФинансирования";
	Запрос.УстановитьПараметр("Организация", Реквизиты.Организация);
	Запрос.УстановитьПараметр("Склад", Реквизиты.Склад);
	Запрос.УстановитьПараметр("ИсточникФинансирования", Реквизиты.ИсточникФинансирования);
	Запрос.Выполнить();
	
	Справочники.КлючиАналитикиВидаУчета.ИнициализироватьКлючиАналитики(Запрос.МенеджерВременныхТаблиц);
	
КонецПроцедуры

#КонецОбласти // Проведение

////////////////////////////////////////////////////////////////////////////////
// Печать
#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	УправлениеПечатьюБольничнаяАптека.ДобавитьКомандыПечати(ПустаяСсылка().Метаданные().ПолноеИмя(), КомандыПечати);
	
КонецПроцедуры

// Возвращает список доступных печатных форм документа
//
Функция ДоступныеПечатныеФормы() Экспорт
	
	ПечатныеФормы = УправлениеПечатьюБольничнаяАптека.СоздатьКоллекциюДоступныхПечатныхФорм();
	
	Возврат ПечатныеФормы;
	
КонецФункции

#КонецОбласти // Печать

////////////////////////////////////////////////////////////////////////////////
// Команды формы
#Область КомандыФормы

// Заполняет список команд ввода на основании.
// 
// Параметры:
//   КомандыСоздатьНаОсновании - ТаблицаЗначений - Таблица команд для вывода в подменю. Для изменения.
//
Процедура ДобавитьКомандыСоздатьНаОсновании(КомандыСоздатьНаОсновании, НастройкиФормы) Экспорт
	
	ВводНаОснованииБольничнаяАптека.ДобавитьКомандыСоздатьНаОсновании(ПустаяСсылка().Метаданные().ПолноеИмя(), КомандыСоздатьНаОсновании, НастройкиФормы);
	
	Команда = ВводНаОснованииБольничнаяАптека.ДобавитьКомандуСоздатьНаОснованииОбъекта(Метаданные.Документы.РазмещениеТоваровПоМестамХранения.ПолноеИмя(), КомандыСоздатьНаОсновании, НастройкиФормы);
	ПодключаемыеКоманды.ДобавитьУсловиеВидимостиКоманды(Команда, "ТипОперации", Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваров);
	
	Команда = ВводНаОснованииБольничнаяАптека.ДобавитьКомандуСоздатьНаОснованииОбъекта(Метаданные.Документы.РазмещениеТоваровПоМестамХраненияВОтделении.ПолноеИмя(), КомандыСоздатьНаОсновании, НастройкиФормы);
	ПодключаемыеКоманды.ДобавитьУсловиеВидимостиКоманды(Команда, "ТипОперации", Перечисления.ТипыОперацийВводаОстатков.ОстаткиСобственныхТоваровВОтделениях);
	
КонецПроцедуры

// Заполняет список команд отчетов.
// 
// Параметры:
//   КомандыОтчетов - ТаблицаЗначений - Таблица команд для вывода в подменю. Для изменения.
//
Процедура ДобавитьКомандыОтчетов(КомандыОтчетов, НастройкиФормы) Экспорт
	
	МенюОтчетыБольничнаяАптека.ДобавитьОбщиеКоманды(ПустаяСсылка().Метаданные().ПолноеИмя(), КомандыОтчетов, НастройкиФормы);
	
КонецПроцедуры

#КонецОбласти // КомандыФормы

#КонецОбласти // СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// СТАНДАРТНЫЕ ПОДСИСТЕМЫ
#Область СтандартныеПодсистемы

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
//
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти // СтандартныеПодсистемы

////////////////////////////////////////////////////////////////////////////////
// ОБНОВЛЕНИЕ ИНФОРМАЦИОННОЙ БАЗЫ
#Область ОбновлениеИнформационнойБазы

Процедура ЗаполнитьПартииОтложенно(Параметры) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
	|	ДанныеДокумента.Ссылка.Дата КАК ДатаДокумента,
	|	ДанныеДокумента.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.ВводОстатков.Товары КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка.Проведен
	|	И ДанныеДокумента.СтатусУказанияПартий <> &СтатусПартииНеУказываются
	|	И ДанныеДокумента.Партия = ЗНАЧЕНИЕ(Справочник.ПартииНоменклатуры.ПустаяСсылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДатаДокумента УБЫВ
	|");
	
	Запрос.УстановитьПараметр("СтатусПартииНеУказываются", ЗапасыКлиентСерверПовтИсп.СтатусыУказанияПартий().СтатусПартииНеУказываются);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		НачатьТранзакцию();
		
		Попытка
			
			// Устанавливаем управляемую блокировку, чтобы провести ответственное чтение объекта.
			Блокировка = Новый БлокировкаДанных;
			
			ЭлементБлокировки = Блокировка.Добавить(Выборка.Ссылка.Метаданные().ПолноеИмя());
			ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
			
			Блокировка.Заблокировать();
			
			ДокументОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
			// Если объект ранее был удален или обработан другими сеансами, пропускаем его.
			Если ДокументОбъект = Неопределено Тогда
				
				ОтменитьТранзакцию();
				Продолжить;
				
			КонецЕсли;
			
			Справочники.ПартииНоменклатуры.ЗаполнитьПартиюВКоллекции(ДокументОбъект, ДокументОбъект.Контрагент, Новый Структура("ЗаменятьВыбраннуюПартию", Истина));
			
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(ДокументОбъект);
			
			ЗафиксироватьТранзакцию();
			
		Исключение
			
			ОтменитьТранзакцию();
			
			ТекстСообщения = НСтр("ru = 'Не удалось обработать документ: %Ссылка% по причине: %Причина%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Ссылка%", Выборка.Ссылка);
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			
			ЗаписьЖурналаРегистрации(
				ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(),
				УровеньЖурналаРегистрации.Предупреждение,
				Метаданные.Документы.ИзготовлениеПоЛекарственнойПрописи,
				Выборка.Ссылка,
				ТекстСообщения);
			
		КонецПопытки;
		
	КонецЦикла;
	
	Параметры.ОбработкаЗавершена = (Выборка.Количество() = 0);
	
КонецПроцедуры

#КонецОбласти // ОбновлениеИнформационнойБазы

#КонецЕсли