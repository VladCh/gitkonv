
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	БизнесПроцессыИЗадачиСервер.ФормаЗадачиПриСозданииНаСервере(
		ЭтотОбъект,
		Объект,
		Элементы.ГруппаСостояние,
		Элементы.ДатаИсполнения);
	
	ТочкиМаршрута = Новый Массив;
	ТочкиМаршрута.Добавить(БизнесПроцессы.Исполнение.ТочкиМаршрута.Исполнить);
	ТочкиМаршрута.Добавить(БизнесПроцессы.Исполнение.ТочкиМаршрута.ОтветственноеИсполнение);
	ТочкиМаршрута.Добавить(БизнесПроцессы.Исполнение.ТочкиМаршрута.Проверить);
	ТочкаМаршрутаОтвИсполнение = БизнесПроцессы.Исполнение.ТочкиМаршрута.ОтветственноеИсполнение;
	
	Исполнители.Параметры.УстановитьЗначениеПараметра("БизнесПроцесс", Объект.БизнесПроцесс);
	Исполнители.Параметры.УстановитьЗначениеПараметра("ТочкиМаршрута", ТочкиМаршрута);
	Исполнители.Параметры.УстановитьЗначениеПараметра("ТочкаМаршрутаОтвИсполнение", ТочкаМаршрутаОтвИсполнение);
	
	БизнесПроцессыИЗадачиСервер.УстановитьФорматДаты(Элементы.ИсполнителиДатаИсполнения);
	БизнесПроцессыИЗадачиСервер.УстановитьФорматДаты(Элементы.ИсполнителиСрокИсполнения);
	БизнесПроцессыИЗадачиСервер.УстановитьОформлениеЗадач(Исполнители.УсловноеОформление);
	
	Если Объект.Ссылка.Пустая() Тогда
		ИнициализацияФормы();
	КонецЕсли;
	
	ТекущийПользователь = Пользователи.ТекущийПользователь();
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ИнициализацияФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	БизнесПроцессыИЗадачиКлиент.ФормаЗадачиОбработкаОповещения(ЭтотОбъект, ИмяСобытия, Параметр, Источник);
	
	Если ИмяСобытия = "ИзмененыРеквизитыНевыполненныхЗадач" И Параметр = Объект.БизнесПроцесс И Не Объект.Выполнена Тогда
		ДатаИсполнения = Объект.ДатаИсполнения;
		Прочитать();
		Объект.ДатаИсполнения = ДатаИсполнения;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьИЗакрытьВыполнить(Команда)
	
	ОчиститьСообщения();
	Если Записать() Тогда
		ОповеститьОбИзменении(Объект.Ссылка);
		Оповестить("ЗадачаИзменена", Объект.Ссылка);
		
		ПоказатьОповещениеПользователя(
			НСтр("ru = 'Изменение:'"), 
			ПолучитьНавигационнуюСсылку(Объект.Ссылка),
			Строка(Объект.Ссылка),
			БиблиотекаКартинок.Информация32);
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьВыполнить(Команда)
	
	Если Записать() Тогда
		ОповеститьОбИзменении(Объект.Ссылка);
		Оповестить("ЗадачаИзменена", Объект.Ссылка);
		
		ПоказатьОповещениеПользователя(
			НСтр("ru = 'Изменение:'"),
			ПолучитьНавигационнуюСсылку(Объект.Ссылка),
			Строка(Объект.Ссылка),
			БиблиотекаКартинок.Информация32);
	КонецЕсли;	
	
КонецПроцедуры

&НаКлиенте
Процедура Проконтролировано(Команда)
	
	ПараметрыЗаписи = Новый Структура;
	ПараметрыЗаписи.Вставить("ВыполнитьЗадачу", Истина);
	
	Если Не Записать(ПараметрыЗаписи) Тогда 
		Возврат;
	КонецЕсли;
	
	ПоказатьОповещениеПользователя(
		НСтр("ru = 'Выполнение:'"),
		ПолучитьНавигационнуюСсылку(Объект.Ссылка),
		Строка(Объект.Ссылка),
		БиблиотекаКартинок.Информация32);
	
	Оповестить("ЗадачаВыполнена", Объект.Ссылка);
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура Дополнительно(Команда)
	
	БизнесПроцессыИЗадачиКлиент.ОткрытьДопИнформациюОЗадаче(Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьБизнесПроцесс(Команда)
	
	ПоказатьЗначение(Неопределено, Объект.БизнесПроцесс);
	
КонецПроцедуры

&НаКлиенте
Процедура ПринятьКИсполнению(Команда)
	
	БизнесПроцессыИЗадачиКлиент.ПринятьЗадачуКИсполнению(ЭтотОбъект, ТекущийПользователь);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьПринятиеКИсполнению(Команда)
	
	БизнесПроцессыИЗадачиКлиент.ОтменитьПринятиеЗадачиКИсполнению(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ПредметНажатие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ПоказатьЗначение(Неопределено, Объект.Предмет);
	
КонецПроцедуры

&НаКлиенте
Процедура ИсполнительСтрокойОткрытие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	Если ЗначениеЗаполнено(Объект.Исполнитель) Тогда
		ПоказатьЗначение(Неопределено, Объект.Исполнитель);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ИнициализацияФормы()
	
	НачальныйПризнакВыполнения = Объект.Выполнена;
	
	БизнесПроцессыИЗадачиБольничнаяАптека.ФормаЗадачиИнициализировать(ЭтотОбъект, Объект, Элементы.СрокИсполнения, Элементы.Предмет);
	
	Если Объект.Выполнена Тогда
		Элементы.ТекстРезультатаВыполнения.Заголовок = НСТР("ru = 'Проконтролировано.'");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции
