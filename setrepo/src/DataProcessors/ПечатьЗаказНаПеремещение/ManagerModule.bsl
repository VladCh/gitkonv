#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Функция возвращает элемент таблицы доступных печатных форм объекта печати
//
Функция ДобавитьПечатнуюФорму(ПечатныеФормы) Экспорт
	
	МетаданныеМакета = МетаданныеМакета();
	МенеджерПечати = МетаданныеМакета.Родитель().ПолноеИмя();
	
	ПечатнаяФорма = УправлениеПечатьюБольничнаяАптека.ДобавитьПечатнуюФорму(ПечатныеФормы, "ЗаказНаПеремещение", МенеджерПечати);
	ПечатнаяФорма.ПутьКМакету = ФормированиеПечатныхФормБольничнаяАптека.ПутьКМакету(МетаданныеМакета);
	ПечатнаяФорма.Представление = МетаданныеМакета.Представление();
	
	УправлениеПечатьюБольничнаяАптека.ДобавитьКомандуПечати(ПечатнаяФорма);
	
	Возврат ПечатнаяФорма;
	
КонецФункции

// Функция формирует печатную форму Заказ на перемещение
//
Функция ПечатьЗаказНаПеремещение(МассивОбъектов, ОбъектыПечати) Экспорт
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	
	ТабличныйДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Портрет;
	ТабличныйДокумент.АвтоМасштаб        = Истина;
	
	ПолноеИмяМакета = ФормированиеПечатныхФормБольничнаяАптека.ПутьКМакету(МетаданныеМакета());
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_" + ПолноеИмяМакета;
	Макет = УправлениеПечатью.МакетПечатнойФормы(ПолноеИмяМакета);
	
	СтруктураТипов = ОбщегоНазначенияБольничнаяАптека.РазложитьМассивСсылокПоТипам(МассивОбъектов);
	
	НомерТипаДокумента = 0;
	Для Каждого СтруктураОбъектов Из СтруктураТипов Цикл
		
		НомерТипаДокумента = НомерТипаДокумента + 1;
		Если НомерТипаДокумента > 1 Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(СтруктураОбъектов.Ключ);
		
		ДанныеДляПечати = МенеджерОбъекта.ПолучитьДанныеДляПечати(СтруктураОбъектов.Значение);
		
		СформироватьТабличныйДокумент(ТабличныйДокумент, Макет, ДанныеДляПечати, ОбъектыПечати);
		
	КонецЦикла;
	
	Возврат ТабличныйДокумент;
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

Процедура СформироватьТабличныйДокумент(ТабличныйДокумент, Макет, ДанныеДляПечати, ОбъектыПечати)
	
	Шапка = ДанныеДляПечати.РезультатПоШапке.Выбрать();
	ВыборкаПоДокументам = ДанныеДляПечати.РезультатПоТабличнойЧасти.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	ПервыйДокумент = Истина;
	Пока Шапка.Следующий() Цикл
		
		Если Не ПервыйДокумент Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		Иначе
			ПервыйДокумент = Ложь;
		КонецЕсли;
		
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		ШтрихкодированиеПечатныхФорм.ВывестиШтрихкодВТабличныйДокумент(Шапка.Ссылка, ТабличныйДокумент, Макет);
		
		// Получение параметров для заполнения
		ДанныеШапки = ПолучитьДанныеШапкиДокумента(Шапка);
		
		// Вывод области Заголовок
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "Заголовок", ДанныеШапки);
		
		// Вывод области РеквизитыШапки
		ОрганизацияПолучатель = Неопределено;
		Если ДанныеШапки.Свойство("ОрганизацияПолучатель", ОрганизацияПолучатель) И ЗначениеЗаполнено(ОрганизацияПолучатель) Тогда
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "РеквизитыЗаголовкаВнутреннееПеремещение", ДанныеШапки);
		Иначе
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "РеквизитыЗаголовка", ДанныеШапки);
		КонецЕсли;
		
		ВыборкаПоДокументам.Сбросить();
		ПараметрыПоиска = Новый Структура("Документ", Шапка.Ссылка);
		Если ВыборкаПоДокументам.НайтиСледующий(ПараметрыПоиска) Тогда
			ВыборкаСтрокТовары = ВыборкаПоДокументам.Выбрать();
		Иначе
			ВыборкаСтрокТовары = Неопределено;
		КонецЕсли;
		
		// Инициализация нумерации
		Нумерация = ИнициализироватьНумерацию();
		
		// Инициализация параметров вывода колонок
		ПараметрыКолонок = ПолучитьПараметрыВыводаКолонок(ВыборкаСтрокТовары);
		
		// Вывод шапки таблицы
		ОбластьКолонкаТовар = Макет.Область("Товар");
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаТаблицы|НомерСтроки");
		Если ПараметрыКолонок.ВыводитьКоды Тогда
			ФормированиеПечатныхФормБольничнаяАптека.ПрисоединитьОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаТаблицы|КолонкаКодов", ПараметрыКолонок);
		Иначе
			ОбластьКолонкаТовар.ШиринаКолонки = ОбластьКолонкаТовар.ШиринаКолонки + Макет.Область("КолонкаКодов").ШиринаКолонки;
		КонецЕсли;
		ФормированиеПечатныхФормБольничнаяАптека.ПрисоединитьОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаТаблицы|Данные");
		
		ТабличныйДокумент.ПовторятьПриПечатиСтроки = ТабличныйДокумент.Область(ТабличныйДокумент.ВысотаТаблицы, ,ТабличныйДокумент.ВысотаТаблицы);
		
		// Вывод многострочной части
		Если ВыборкаСтрокТовары <> Неопределено Тогда
			
			КоличествоСтрок = ВыборкаСтрокТовары.Количество();
			КлючиПараметров = ФормированиеПечатныхФормБольничнаяАптека.ПолучитьИменаКолонокТаблицы(ВыборкаСтрокТовары);
			Пока ВыборкаСтрокТовары.Следующий() Цикл
				
				ФормированиеПечатныхФормБольничнаяАптека.УвеличитьНомер(Нумерация, "НомерСтроки");
				
				ДанныеСтроки = Новый Структура(КлючиПараметров);
				ЗаполнитьЗначенияСвойств(ДанныеСтроки, ВыборкаСтрокТовары);
				
				ДанныеСтроки.Вставить("НомерСтроки"      , Нумерация.НомерСтроки);
				ДанныеСтроки.Вставить("ТоварНаименование", ПолучитьПредставлениеНоменклатуры(ВыборкаСтрокТовары));
				
				ОбластьДанных = Макет.ПолучитьОбласть("Строка|Данные");
				ОбластьДанных.Параметры.Заполнить(ДанныеСтроки);
				ЭтоПоследняяСтрока = Нумерация.НомерСтроки = КоличествоСтрок;
				ПроверитьВыводСтроки(ТабличныйДокумент, Макет, ОбластьДанных, ЭтоПоследняяСтрока);
				
				ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "Строка|НомерСтроки", ДанныеСтроки);
				Если ПараметрыКолонок.ВыводитьКоды Тогда
					ФормированиеПечатныхФормБольничнаяАптека.ПрисоединитьОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаТаблицы|КолонкаКодов", ДанныеСтроки);
				КонецЕсли;
				ТабличныйДокумент.Присоединить(ОбластьДанных);
				
			КонецЦикла;
		КонецЕсли;
		
		// Вывод подвала
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "Подвал");
		
		// Вывод подписей
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "Подписи", ДанныеШапки);
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
		
	КонецЦикла;
	
КонецПроцедуры

Функция ПолучитьДанныеШапкиДокумента(Шапка)
	
	КлючиПараметров = ФормированиеПечатныхФормБольничнаяАптека.ПолучитьИменаКолонокТаблицы(Шапка);
	
	Параметры = Новый Структура(КлючиПараметров);
	ЗаполнитьЗначенияСвойств(Параметры, Шапка);
	
	// Данные заголовка
	НомерДокумента = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.НомерДокумента);
	ТекстЗаголовка = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Заказ на перемещение № %1 от %2'"), НомерДокумента, Формат(Шапка.ДатаДокумента, "ДЛФ=DD"));
	
	Параметры.Вставить("ТекстЗаголовка", ТекстЗаголовка);
	
	// Данные реквизитов шапки
	СведенияОбОрганизации = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Шапка.Организация, Шапка.ДатаДокумента);
	Параметры.Вставить("ОрганизацияПредставление", ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОбОрганизации));
	
	ОрганизацияПолучатель = Неопределено;
	Если Параметры.Свойство("ОрганизацияПолучатель", ОрганизацияПолучатель) И ЗначениеЗаполнено(ОрганизацияПолучатель) Тогда
		СведенияОбОрганизацииПолучателе = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(ОрганизацияПолучатель, Шапка.ДатаДокумента);
		Параметры.Вставить("ОрганизацияПолучательПредставление", ФормированиеПечатныхФорм.ОписаниеОрганизации(СведенияОбОрганизацииПолучателе));
	КонецЕсли;
	
	// Данные подписей документа
	Параметры.Вставить("ФИОМенеджер", ФизическиеЛицаКлиентСервер.ФамилияИнициалы(СокрЛП(Шапка.Менеджер)));
	
	Возврат Параметры;
	
КонецФункции

Функция ИнициализироватьНумерацию()
	
	Нумерация = Новый Структура;
	Нумерация.Вставить("НомерСтроки", 0);
	
	Возврат Нумерация;
	
КонецФункции

Функция ПолучитьПараметрыВыводаКолонок(Товары)
	
	ПараметрыКолонок = Новый Структура;
	ПараметрыКолонок.Вставить("ВыводитьКоды", Ложь);
	
	Если ПараметрыКолонок.ВыводитьКоды Тогда
		ПараметрыКолонок.Вставить("ИмяКолонкиКодов", "");
	КонецЕсли;
	
	Возврат ПараметрыКолонок;
	
КонецФункции

Функция ПолучитьПредставлениеНоменклатуры(СтрокаТовары)
	
	Если ЗначениеЗаполнено(СтрокаТовары.Номенклатура) Тогда
		ТоварНаименование = ОбщегоНазначенияБольничнаяАптека.ПолучитьПредставлениеНоменклатурыДляПечати(СтрокаТовары.ТоварНаименование);
	Иначе
		
		Если ЗначениеЗаполнено(СтрокаТовары.ТорговоеНаименование) Тогда
			ТоварНаименование = СокрЛП(СтрокаТовары.ТорговоеНаименование);
		Иначе
			ТоварНаименование = СокрЛП(СтрокаТовары.ДействующиеВеществаМНН);
		КонецЕсли;
		
		УпаковкаПрепарата = ?(СтрокаТовары.ТипЕдиницы = Перечисления.ТипыЕдиницИзмерения.Упаковка, ", " + СокрЛП(СтрокаТовары.ЕдиницаИзмеренияНаименование), "");
		ТоварНаименование = ТоварНаименование + ", " + СокрЛП(СтрокаТовары.ФормаВыпуска) + УпаковкаПрепарата;
		
	КонецЕсли;
	
	Возврат ТоварНаименование;
	
КонецФункции

Процедура ПроверитьВыводСтроки(ТабличныйДокумент, Макет, ТекущаяОбласть, ЭтоПоследняяСтрока)
	
	МассивВыводимыхОбластей = Новый Массив;
	МассивВыводимыхОбластей.Добавить(ТекущаяОбласть);
	Если ЭтоПоследняяСтрока Тогда
		МассивВыводимыхОбластей.Добавить(Макет.ПолучитьОбласть("Подвал"));
		МассивВыводимыхОбластей.Добавить(Макет.ПолучитьОбласть("Подписи"));
	КонецЕсли;
	
	Если Не ОбщегоНазначения.ПроверитьВыводТабличногоДокумента(ТабличныйДокумент, МассивВыводимыхОбластей) Тогда
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
	КонецЕсли;
	
КонецПроцедуры

Функция МетаданныеМакета()
	
	Возврат Метаданные.Обработки.ПечатьЗаказНаПеремещение.Макеты.ПФ_MXL_ЗаказНаПеремещение;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции

#КонецЕсли
