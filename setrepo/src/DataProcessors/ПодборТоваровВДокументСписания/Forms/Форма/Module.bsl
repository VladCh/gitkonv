
////////////////////////////////////////////////////////////////////////////////
// ОПИСАНИЕ ПЕРЕМЕННЫХ
#Область ОписаниеПеременных

&НаКлиенте
Перем КэшированныеЗначения;

#КонецОбласти // ОписаниеПеременных

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформлениеФормы();
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Если Параметры.Документ = Неопределено Тогда
		ВызватьИсключение НСтр("ru='Предусмотрено открытие обработки только из документов.'");
	КонецЕсли;
	
	ЗаполнениеПараметровФормы = Новый Структура;
	ЗаполнениеПараметровФормы.Вставить("ФормаПодбора");
	ЗаполнениеПараметровФормы.Вставить("ПодборТоваров");
	ЗаполнениеПараметровФормы.Вставить("ПодборВДокументСписания");
	
	ПараметрыФормы = Новый ФиксированнаяСтруктура(ЗаполнениеПараметровФормы);
	
	КодФормы = "Обработка_ПодборТоваровВДокументСписания_Форма";
	
	ПодборТоваровСервер.ПриСозданииФормыПодбораНаСервере(ЭтотОбъект);
	ПодборТоваровСервер.УстановитьЗаголовокФормыПодбора(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Элементы.ГруппаПользовательскихНастроек.ПодчиненныеЭлементы.Количество() > 0 Тогда
		НастроитьОтображениеПользовательскихНастроек();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Если Объект.Корзина.Количество() > 0 Тогда
		Оповещение = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
		ПодборТоваровКлиент.ПередЗакрытиемФормыПодбора(Оповещение, Отказ, ЗавершениеРаботы,, ТекстПредупреждения);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	ПеренестиВДокументДанныеПодбора();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	Если Не ЗавершениеРаботы Тогда
		СохранитьНастройкиФормы();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗапрашиватьКоличество(Команда)
	
	ЗапрашиватьКоличество = Не ЗапрашиватьКоличество;
	ОбщегоНазначенияБольничнаяАптекаКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ЗапрашиватьКоличество", "Пометка", ЗапрашиватьКоличество);
	
КонецПроцедуры

&НаКлиенте
Процедура НастроитьПоиск(Команда)
	
	ПодборТоваровКлиент.НастроитьПоиск(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПеренестиВДокумент(Команда)
	
	ПеренестиВДокументДанныеПодбора();
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьКарточкуТовара(Команда)
	
	ТекущаяСтрока = ПодборТоваровКлиентСервер.ТекущийСписокТоваров(ЭтотОбъект).ТекущиеДанные;
	Если ТекущаяСтрока <> Неопределено Тогда
		ПоказатьЗначение(, ТекущаяСтрока.Номенклатура);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// Отборы формы
#Область ОтборыФормы

&НаКлиенте
Процедура ТолькоВНаличииПриИзменении(Элемент)
	
	ОбработатьИзменениеПризнакаТолькоВНаличии();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборОстатковБезЗабракованныхИПросроченныхПриИзменении(Элемент)
	
	ОбработатьИзменениеОтбораОстатковБезЗабракованныхИПросроченных();
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборИсточникФинансированияПриИзменении(Элемент)
	
	ОбработатьИзменениеОтбораОстатковПоИсточникуФинансирования();
	
КонецПроцедуры

&НаКлиенте
Процедура ИсточникФинансированияПриИзменении(Элемент)
	
	ОбработатьИзменениеОтбораОстатковПоИсточникуФинансирования(Истина);
	
КонецПроцедуры

#КонецОбласти // ОтборыФормы

////////////////////////////////////////////////////////////////////////////////
// Список "Список товаров"
#Область СписокТоваров

&НаКлиенте
Процедура СписокТоваровПриАктивизацииСтроки(Элемент)
	
	// ФильтрНоменклатурыПоИерархии
	ФильтрНоменклатурыПоИерархииКлиент.ПриАктивизацииСтрокиСпискаНоменклатуры(ЭтотОбъект);
	// Конец ФильтрНоменклатурыПоИерархии
	
КонецПроцедуры

&НаКлиенте
Процедура СписокТоваровВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ПодборТоваровКлиент.ПриВыбореСтрокиТаблицыНоменклатуры(ЭтотОбъект, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти // СписокТоваров

////////////////////////////////////////////////////////////////////////////////
// Список "Корзина"
#Область СписокКорзина

&НаКлиенте
Процедура КоличествоПодобраноИнформацияНажатие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	ПодборТоваровКлиент.ПриНажатииНаИнформациюОПодобранныхТоварах(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаПриИзменении(Элемент)
	
	ПодборТоваровКлиентСервер.УстановитьТекстИнформационнойНадписи(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаНоменклатураПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьСериюНоменклатурыПоВладельцу(), ТекущаяСтрока.СерияНоменклатуры);
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьПартиюПоВладельцу(), ТекущаяСтрока.Партия);
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьУпаковкуПоВладельцу(), ТекущаяСтрока.ЕдиницаИзмерения);
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЕдиницуИзмерения());
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьПараметрыУчета(), ПараметрыУчетаНоменклатуры);
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЦенуПродажи(), Действия.ПолучитьПараметрыЗаполненияЦены(ЭтотОбъект));
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаСерияНоменклатурыПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПроверитьПартиюПоВладельцу(), ТекущаяСтрока.Партия);
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЦенуПродажи(), Действия.ПолучитьПараметрыЗаполненияЦены(ЭтотОбъект));
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаПартияНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ТекущаяСтрока = Элемент.Родитель.ТекущиеДанные;
	ОтборПартий = Новый Структура;
	ОтборПартий.Вставить("Документ"     , Параметры.Документ);
	ОтборПартий.Вставить("Организация"  , Организация);
	ОтборПартий.Вставить("Склад"        , Склад);
	
	ПараметрыВыбораПартии = ОбработкаТабличнойЧастиКлиент.ПолучитьПараметрыВыбораПартии(ОтборПартий, ТекущаяСтрока);
	ПараметрыВыбораПартии.ВыборВОтделении = ПодборТоваровКлиентСервер.ПодборВОтделении(ЭтотОбъект);
	
	ОбработкаТабличнойЧастиКлиент.ВыбратьПартиюНоменклатуры(ЭтотОбъект, Элемент, ПараметрыВыбораПартии, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаПартияПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЦенуПродажи(), Действия.ПолучитьПараметрыЗаполненияЦены(ЭтотОбъект));
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаЕдиницаИзмеренияПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	Если ТекущаяСтрока.Количество > 0 Тогда
		СтруктураДействий.Вставить(Действия.Действие_ПересчитатьЦенуЗаУпаковку(), Действия.ПолучитьПараметрыПересчетаЦеныЗаУпаковку(ТекущаяСтрока.Количество));
	Иначе
		СтруктураДействий.Вставить(Действия.Действие_ЗаполнитьЦенуПродажи(), Действия.ПолучитьПараметрыЗаполненияЦены(ЭтотОбъект));
		СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	КонецЕсли;
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаКоличествоПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоэффициент());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьКоличествоЕдиниц());
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаЦенаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьСумму());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

&НаКлиенте
Процедура КорзинаСуммаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Корзина.ТекущиеДанные;
	
	Действия = ОбработкаТабличнойЧастиКлиентСервер;
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить(Действия.Действие_ПересчитатьЦену());
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТабличнойЧасти(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

#КонецОбласти // СписокКорзина

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформлениеФормы()
	
	ОбработкаТабличнойЧастиСервер.УстановитьОформлениеСерийНоменклатуры(ЭтотОбъект, "КорзинаСерияНоменклатуры", "Объект.Корзина.СтатусУказанияСерий");
	ОбработкаТабличнойЧастиСервер.УстановитьОформлениеПартий(ЭтотОбъект, "КорзинаПартия", "Объект.Корзина.СтатусУказанияПартий");
	
КонецПроцедуры

&НаКлиенте
Процедура ПеренестиВДокументДанныеПодбора()
	
	АдресТоваровВХранилище = ПоместитьОтобранныеТоварыВХранилище();
	ДанныеПодбора = Новый Структура("АдресТоваровВХранилище", АдресТоваровВХранилище);
	
	ЗакрыватьПриВыборе = Ложь;
	ОповеститьОВыборе(ДанныеПодбора);
	
	ЗакрытьФормуБезПодтверждения = Истина;
	Закрыть(ДанныеПодбора);
	
КонецПроцедуры

&НаСервере
Функция ПоместитьОтобранныеТоварыВХранилище() 
	
	Возврат ПоместитьВоВременноеХранилище(Объект.Корзина.Выгрузить(), УникальныйИдентификатор);

КонецФункции

&НаСервере
Процедура СохранитьНастройкиФормы()
	
	ПодборТоваровСервер.СохранитьНастройкиФормы(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеПризнакаТолькоВНаличии()
	
	ПодборТоваровСервер.УстановитьОтборТолькоВНаличии(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеОтбораОстатковБезЗабракованныхИПросроченных()
	
	ПодборТоваровСервер.УстановитьОтборОстатковБезЗабракованныхИПросроченных(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьИзменениеОтбораОстатковПоИсточникуФинансирования(Знач ИзменитьПризнакОтбора = Ложь)
	
	Если ИзменитьПризнакОтбора Тогда
		ОтборИсточникФинансирования = ЗначениеЗаполнено(ИсточникФинансирования);
	КонецЕсли;
	
	ПодборТоваровСервер.УстановитьОтборОстатковПоИсточникуФинансирования(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура НастроитьОтображениеПользовательскихНастроек()
	
	ПодборТоваровСервер.НастроитьОтображениеПользовательскихНастроек(Элементы.ГруппаПользовательскихНастроек);
	
КонецПроцедуры

#КонецОбласти // СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// Расширенный поиск в списке номенклатуры
#Область РасширенныйПоискВСпискеНоменклатуры

&НаКлиенте
Процедура Подключаемый_РасширенныйПоискВСписках_СтрокаПоискаПриИзменении(Элемент)
	
	ВыполнитьПоиск();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_РасширенныйПоискВСписках_СтрокаПоискаОчистка(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	РасширенныйПоискВСпискахКлиентСервер.СнятьОтборПоСтрокеПоиска(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_РасширенныйПоискВСписках_НайтиПоТочномуСоответствиюПриИзменении(Элемент)
	
	ВыполнитьПоиск();
	
КонецПроцедуры

&НаКлиенте
Процедура ВыполнитьПоиск()
	
	ВыполнитьПоискНаСервере();
	
	РасширенныйПоискВСпискахКлиент.ПослеВыполненияПоиска(ЭтотОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ВыполнитьПоискНаСервере()
	
	РасширенныйПоискВСписках.ВыполнитьПоиск(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти // РасширенныйПоискВСпискеНоменклатуры

////////////////////////////////////////////////////////////////////////////////
// Фильтры списка номенклатуры
#Область ФильтрыСпискаНоменклатуры

&НаКлиенте
Процедура Подключаемый_ПодборТоваров_ОтфильтроватьПоАналогичнымСвойствам(Команда)
	
	ТекущаяСтрока = ПодборТоваровКлиентСервер.ТекущийСписокТоваров(ЭтотОбъект).ТекущиеДанные;
	Если ТекущаяСтрока = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ОтфильтроватьПоАналогичнымСвойствам(ТекущаяСтрока.Ссылка);
	
КонецПроцедуры

&НаСервере
Процедура ОтфильтроватьПоАналогичнымСвойствам(Знач Номенклатура)
	
	ПодборТоваровСервер.ОтфильтроватьПоАналогичнымСвойствам(ЭтотОбъект, Номенклатура);
	ФильтрыСписковКлиентСервер.ФильтруемыйСписокЭлементФормы(ЭтотОбъект).ТекущаяСтрока = Номенклатура;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрыСписков_ИспользоватьФильтрыПриИзменении(Элемент)
	
	ФильтрыСписков_ИспользоватьФильтрыПриИзменении();
	// ФильтрНоменклатурыПоИерархии
	ФильтрНоменклатурыПоИерархииКлиент.ПриАктивизацииСтрокиСпискаНоменклатуры(ЭтотОбъект);
	// Конец ФильтрНоменклатурыПоИерархии
	
КонецПроцедуры

&НаСервере
Процедура ФильтрыСписков_ИспользоватьФильтрыПриИзменении()
	
	ФильтрыСписков.ИспользоватьФильтрыПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрыСписков_ВариантФильтраПриИзменении(Элемент)
	
	Если ФильтрыСписковКлиент.НуженСерверныйВызовПриИзмененииВариантаФильтра(ЭтотОбъект) Тогда
		ФильтрыСписков_ВариантФильтраПриИзменении();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ФильтрыСписков_ВариантФильтраПриИзменении()
	
	ФильтрыСписков.ВариантФильтраПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрыСписков_ВариантФильтраОчистка(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Фильтр номенклатуры по иерархии
#Область ФильтрНоменклатурыПоИерархии

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоИерархии_УстановитьТекущуюСтрокуИерархииНоменклатуры()
	
	ФильтрНоменклатурыПоИерархииКлиент.УстановитьТекущуюСтрокуИерархииНоменклатуры(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоИерархии_ИерархияНоменклатурыПриАктивизацииСтроки(Элемент)
	
	ФильтрНоменклатурыПоИерархииКлиент.ПриАктивизацииСтрокиИерархииНоменклатуры(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоИерархии_ОбработатьАктивациюСтрокиИерархииНоменклатуры()
	
	ФильтрНоменклатурыПоИерархииКлиент.ОбработатьАктивациюСтрокиИерархииНоменклатуры(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти // ФильтрНоменклатурыПоИерархии

////////////////////////////////////////////////////////////////////////////////
// Фильтр номенклатуры лекарственных средств
#Область ФильтрНоменклатурыЛекарственныхСредств

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыЛекарственныхСредств_СброситьОтборы(Команда)
	
	ФильтрНоменклатурыЛекарственныхСредств_СброситьОтборы();
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыЛекарственныхСредств_СброситьОтборы()
	
	ФильтрНоменклатурыЛекарственныхСредств.СброситьОтборы(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыЛекарственныхСредств_СписокОтбораНажатие(Элемент, СтандартнаяОбработка)
	
	ФильтрНоменклатурыЛекарственныхСредствКлиент.ВыбратьСписокОтбора(ЭтотОбъект, Элемент.Имя, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииФлажкаОтбора(Элемент)
	
	ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииФлажкаОтбора(Элемент.Имя);
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииФлажкаОтбора(Знач ИмяФлажка)
	
	ФильтрНоменклатурыЛекарственныхСредств.ПриИзмененииФлажкаОтбора(ЭтотОбъект, ИмяФлажка);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииЗначенияОтбора(Элемент)
	
	ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииЗначенияОтбора(Элемент.Имя);
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииЗначенияОтбора(ИмяЭлемента)
	
	ФильтрНоменклатурыЛекарственныхСредств.ПриИзмененииЗначенияОтбора(ЭтотОбъект, ИмяЭлемента);
	
КонецПроцедуры

&НаКлиенте
Процедура ФильтрНоменклатурыЛекарственныхСредств_ОбработатьВыборЭлементовСписка(СписокВыбора, ИмяСписка) Экспорт
	
	Если ФильтрНоменклатурыЛекарственныхСредствКлиент.ОбработатьВыборЭлементовСписка(ЭтотОбъект, СписокВыбора, ИмяСписка) Тогда
		ФильтрНоменклатурыЛекарственныхСредств_ПриИзмененииЗначенияОтбора(ЭтотОбъект.ТекущийЭлемент.Имя);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыЛекарственныхСредств_ФормаВыпускаНажатие(Элемент, СтандартнаяОбработка)
	
	ФильтрНоменклатурыЛекарственныхСредствКлиент.УстановитьОтборПоФормеВыпуска(ЭтотОбъект, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ФильтрНоменклатурыЛекарственныхСредств_ОбработатьУстановкуОтбораПоФормеВыпуска(Отбор, ДополнительныеПараметры) Экспорт
	
	Если Отбор <> Неопределено Тогда
		ФильтрНоменклатурыЛекарственныхСредств_ОбработатьУстановкуОтбораПоФормеВыпускаСервер(Отбор);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыЛекарственныхСредств_ОбработатьУстановкуОтбораПоФормеВыпускаСервер(Знач Отбор)
	
	ФильтрНоменклатурыЛекарственныхСредств.УстановитьОтборыСписка(ЭтотОбъект, Отбор);
	
КонецПроцедуры

#КонецОбласти // ФильтрНоменклатурыЛекарственныхСредств

////////////////////////////////////////////////////////////////////////////////
// Фильтр номенклатуры по виду и свойствам
#Область ПоВидуИСвойствам

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоВидуИСвойствам_СброситьФильтрПоСвойствам(Команда)
	
	ФильтрНоменклатурыПоВидуИСвойствам_СброситьФильтрПоСвойствам();
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыПоВидуИСвойствам_СброситьФильтрПоСвойствам()
	
	ФильтрНоменклатурыПоВидуИСвойствам.СброситьФильтрПоСвойствам(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоВидуИСвойствам_ПриИзмененииВидаНоменклатуры(Элемент)
	
	ФильтрНоменклатурыПоВидуИСвойствам_ПриИзмененииВидаНоменклатуры();
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыПоВидуИСвойствам_ПриИзмененииВидаНоменклатуры()
	
	ФильтрНоменклатурыПоВидуИСвойствам.ПриИзмененииВидаНоменклатуры(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоВидуИСвойствам_ПоказатьСкрытьВидыНоменклатурыНажатие(Элемент)
	
	ФильтрНоменклатурыПоВидуИСвойствамКлиент.ПоказатьСкрытьВидыНоменклатуры(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоВидуИСвойствам_ВидыНоменклатурыПриАктивизацииСтроки(Элемент)
	
	ФильтрНоменклатурыПоВидуИСвойствамКлиент.ПриАктивизацииСтрокиВидаНоменклатуры(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоИерархии_ОбработатьАктивациюСтрокиВидаНоменклатуры()
	
	ФильтрНоменклатурыПоВидуИСвойствам_ПриИзмененииВидаНоменклатуры();
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоВидуИСвойствам_ФильтрПоСвойствамВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ФильтрНоменклатурыПоВидуИСвойствамКлиент.ФильтрПоСвойствамВыбор(ЭтотОбъект, Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ФильтрНоменклатурыПоВидуИСвойствам_ФильтрПоСвойствамОтборПриИзменении(Элемент)
	
	ФильтрНоменклатурыПоВидуИСвойствамКлиент.ФильтрПоСвойствамОтборПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ФильтрНоменклатурыПоВидуИСвойствам_ФильтрПоСвойствамПриИзмененииОтбора() Экспорт
	
	ФильтрНоменклатурыПоВидуИСвойствам_ФильтрПоСвойствамПриИзмененииОтбораНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ФильтрНоменклатурыПоВидуИСвойствам_ФильтрПоСвойствамПриИзмененииОтбораНаСервере()
	
	ФильтрНоменклатурыПоВидуИСвойствам.ФильтрПоСвойствамПриИзменении(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти // ПоВидуИСвойствам

&НаКлиенте
Процедура Подключаемый_ПанельОтборов_СвернутьРазвернутьОтбор(Элемент)
	
	ПанельОтборовКлиентСервер.СкрытьПоказатьПанельОтборов(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти // ФильтрыСпискаНоменклатуры
