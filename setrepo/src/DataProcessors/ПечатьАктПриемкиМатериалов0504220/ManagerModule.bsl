#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

// Функция возвращает элемент таблицы доступных печатных форм объекта печати
//
Функция ДобавитьПечатнуюФорму(ПечатныеФормы) Экспорт
	
	МетаданныеМакета = МетаданныеМакета();
	МенеджерПечати = МетаданныеМакета.Родитель().ПолноеИмя();
	
	ПечатнаяФорма = УправлениеПечатьюБольничнаяАптека.ДобавитьПечатнуюФорму(ПечатныеФормы, "АктПриемкиМатериалов0504220", МенеджерПечати);
	ПечатнаяФорма.ПутьКМакету = ФормированиеПечатныхФормБольничнаяАптека.ПутьКМакету(МетаданныеМакета);
	ПечатнаяФорма.Представление = МетаданныеМакета.Представление();
	
	УправлениеПечатьюБольничнаяАптека.ДобавитьКомандуПечати(ПечатнаяФорма);
	
	Возврат ПечатнаяФорма;
	
КонецФункции

// Функция формирует печатную форму требование накладная ф.0504204
//
Функция ПечатьАктПриемкиМатериалов0504220(МассивОбъектов, ОбъектыПечати) Экспорт
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	
	ТабличныйДокумент.ОриентацияСтраницы = ОриентацияСтраницы.Ландшафт;
	ТабличныйДокумент.АвтоМасштаб        = Истина;
	
	ПолноеИмяМакета = ФормированиеПечатныхФормБольничнаяАптека.ПутьКМакету(МетаданныеМакета());
	ТабличныйДокумент.ИмяПараметровПечати = "ПАРАМЕТРЫ_ПЕЧАТИ_" + ПолноеИмяМакета;
	Макет = УправлениеПечатью.МакетПечатнойФормы(ПолноеИмяМакета);
	
	СтруктураТипов = ОбщегоНазначенияБольничнаяАптека.РазложитьМассивСсылокПоТипам(МассивОбъектов);
	
	НомерТипаДокумента = 0;
	Для Каждого СтруктураОбъектов Из СтруктураТипов Цикл
		
		НомерТипаДокумента = НомерТипаДокумента + 1;
		Если НомерТипаДокумента > 1 Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		
		МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(СтруктураОбъектов.Ключ);
		
		ДанныеДляПечати = МенеджерОбъекта.ПолучитьДанныеДляПечати(СтруктураОбъектов.Значение);
		
		СформироватьТабличныйДокумент(ТабличныйДокумент, Макет, ДанныеДляПечати, ОбъектыПечати);
		
	КонецЦикла;
	
	Возврат ТабличныйДокумент;
	
КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

Процедура СформироватьТабличныйДокумент(ТабличныйДокумент, Макет, ДанныеДляПечати, ОбъектыПечати)
	
	МассивВыводимыхОбластей = Новый Массив;
	
	Шапка = ДанныеДляПечати.ОсновныеДанные;
	ВыборкаПоДокументам = ДанныеДляПечати.ВыборкаПоДокументам;
	
	ПервыйДокумент = Истина;
	Пока Шапка.Следующий() Цикл
		
		// Поиск данных выборки
		ВыборкаПоДокументам.Сбросить();
		СтруктураПоиска = Новый Структура("Ссылка", Шапка.Ссылка);
		НайденСледующий = ВыборкаПоДокументам.НайтиСледующий(СтруктураПоиска);
		
		Если НайденСледующий Тогда
			ВыборкаСтрокТовары = ВыборкаПоДокументам.Выбрать();
		Иначе
			Продолжить;
		КонецЕсли;
		
		Если Не ПервыйДокумент Тогда
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		Иначе
			ПервыйДокумент = Ложь;
		КонецЕсли;
		
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		// Получение параметров для заполнения
		ДанныеШапки = ПолучитьДанныеШапкиДокумента(Шапка);
		
		// Вывод области Заголовок
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьСоШтрихкодом(ТабличныйДокумент, Макет, "Заголовок", ДанныеШапки);
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел1");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаРаздел1");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел1", ДанныеШапки);
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел2");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаРаздел2");
		
		ОбластьСтрокаРаздел2 = Макет.ПолучитьОбласть("СтрокаРаздел2");
		Пока ОбщегоНазначения.ПроверитьВыводТабличногоДокумента(ТабличныйДокумент, ОбластьСтрокаРаздел2) Цикл
			ТабличныйДокумент.Вывести(ОбластьСтрокаРаздел2);
		КонецЦикла;
		
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел3");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаРаздел3");
		Для Номер = 1 По 3 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел3");
		КонецЦикла;
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел4");
		Для Номер = 1 По 8 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел4");
		КонецЦикла;
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел5");
		Для Номер = 1 По 9 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел5");
		КонецЦикла;
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел6");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел6");
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел7");
		Для Номер = 1 По 14 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел7");
		КонецЦикла;
		
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел8");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаРаздел8");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел8", ВыборкаПоДокументам);
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРаздел9");
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ШапкаРаздел9");
		
		// Инициализация итогов
		ПараметрыИтогоРаздел9 = Новый Структура;
		ПараметрыИтогоРаздел9.Вставить("КоличествоПоДокументу", 0);
		ПараметрыИтогоРаздел9.Вставить("СуммаПоДокументу"     , 0);
		ПараметрыИтогоРаздел9.Вставить("Количество"           , 0);
		ПараметрыИтогоРаздел9.Вставить("Сумма"                , 0);
		ПараметрыИтогоРаздел9.Вставить("СуммаИзлишки"         , 0);
		ПараметрыИтогоРаздел9.Вставить("КоличествоБой"        , 0);
		ПараметрыИтогоРаздел9.Вставить("СуммаБой"             , 0);
		ПараметрыИтогоРаздел9.Вставить("КоличествоНедостача"  , 0);
		ПараметрыИтогоРаздел9.Вставить("СуммаНедостача"       , 0);
		
		КлючиПараметров = ФормированиеПечатныхФормБольничнаяАптека.ПолучитьИменаКолонокТаблицы(ВыборкаСтрокТовары);
		
		ОбластьСтрокаРаздел9 = Макет.ПолучитьОбласть("СтрокаРаздел9");
		
		КоличествоСтрок = ВыборкаСтрокТовары.Количество();
		НомерСтроки = 0;
		Пока ВыборкаСтрокТовары.Следующий() Цикл
			
			НомерСтроки = НомерСтроки + 1;
			
			ДанныеСтрокиРаздел9 = Новый Структура(КлючиПараметров);
			ЗаполнитьЗначенияСвойств(ДанныеСтрокиРаздел9, ВыборкаСтрокТовары);
			
			ТоварНаименование = ОбщегоНазначенияБольничнаяАптека.ПолучитьПредставлениеНоменклатурыДляПечати(
				ВыборкаСтрокТовары.ТоварНаименование,
				ВыборкаСтрокТовары.Серия);
			
			КоличествоБойБрак = ВыборкаСтрокТовары.КоличествоБой + ВыборкаСтрокТовары.КоличествоБрак;
			
			ДанныеСтрокиРаздел9.Вставить("ТоварНаименование", ТоварНаименование);
			ДанныеСтрокиРаздел9.Вставить("Цена"             , ВыборкаСтрокТовары.ЦенаСНДС);
			ДанныеСтрокиРаздел9.Вставить("Сумма"            , ВыборкаСтрокТовары.СуммаСНДС);
			ДанныеСтрокиРаздел9.Вставить("СуммаИзлишки"     , ВыборкаСтрокТовары.КоличествоИзлишки * ВыборкаСтрокТовары.ЦенаСНДС);
			ДанныеСтрокиРаздел9.Вставить("КоличествоБой"    , КоличествоБойБрак);
			ДанныеСтрокиРаздел9.Вставить("СуммаБой"         , КоличествоБойБрак * ВыборкаСтрокТовары.ЦенаСНДС);
			ДанныеСтрокиРаздел9.Вставить("СуммаНедостача"   , ВыборкаСтрокТовары.КоличествоНедостача * ВыборкаСтрокТовары.ЦенаСНДС);
			
			ОбластьСтрокаРаздел9.Параметры.Заполнить(ДанныеСтрокиРаздел9);
			
			ЭтоПоследняяСтрока = НомерСтроки = КоличествоСтрок;
			ПроверитьВыводСтроки(ТабличныйДокумент, Макет, МассивВыводимыхОбластей, ОбластьСтрокаРаздел9, ЭтоПоследняяСтрока);
			
			ТабличныйДокумент.Вывести(ОбластьСтрокаРаздел9);
			
			ФормированиеПечатныхФормБольничнаяАптека.РассчитатьИтоги(ДанныеСтрокиРаздел9, ПараметрыИтогоРаздел9);
			
		КонецЦикла;
		
		МассивВыводимыхОбластей = Новый Массив;
		МассивВыводимыхОбластей.Добавить(Макет.ПолучитьОбласть("СтрокаРаздел9"));
		МассивВыводимыхОбластей.Добавить(Макет.ПолучитьОбласть("ИтогоРаздел9"));
		
		Пока ОбщегоНазначения.ПроверитьВыводТабличногоДокумента(ТабличныйДокумент, МассивВыводимыхОбластей) Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРаздел9");
		КонецЦикла;
		
		// Вывод области ИтогоРаздел9
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ИтогоРаздел9", ПараметрыИтогоРаздел9);
		
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРазделЗаключениеКомиссии");
		Для Номер = 1 По 11 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРазделЗаключениеКомиссии");
		КонецЦикла;
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ЗаголовокРазделПереченьПрилагаемыхДокументов");
		Для Номер = 1 По 7 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СтрокаРазделПереченьПрилагаемыхДокументов");
		КонецЦикла;
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "ПредупреждениеОбОтветственности");
		
		Для Номер = 1 По 4 Цикл
			ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "СоставКомиссии");
		КонецЦикла;
		
		ФормированиеПечатныхФормБольничнаяАптека.ВывестиОбластьПоИмени(ТабличныйДокумент, Макет, "Подписи");
		
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент, НомерСтрокиНачало, ОбъектыПечати, Шапка.Ссылка);
		
	КонецЦикла;
	
КонецПроцедуры

Функция ПолучитьДанныеШапкиДокумента(Шапка)
	
	КлючиПараметров = ФормированиеПечатныхФормБольничнаяАптека.ПолучитьИменаКолонокТаблицы(Шапка);
	
	Параметры = Новый Структура(КлючиПараметров);
	ЗаполнитьЗначенияСвойств(Параметры, Шапка);
	
	ОтветственныеЛица = ОтветственныеЛицаСервер.ПолучитьОтветственныеЛицаОрганизации(Шапка.Организация, Шапка.ДатаДокумента);
	
	ПредставлениеВходящегоДокумента = "";
	Если ЗначениеЗаполнено(Шапка.НомерВходящегоДокумента) Тогда
		ПредставлениеВходящегоДокумента = ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.НомерВходящегоДокумента);
	КонецЕсли;
	Если ЗначениеЗаполнено(Шапка.ДатаВходящегоДокумента) Тогда
		ПредставлениеВходящегоДокумента = ПредставлениеВходящегоДокумента + " от " + Формат(Шапка.ДатаВходящегоДокумента,"ДЛФ=D");
	КонецЕсли;
	
	Параметры.Вставить("НомерДокумента"                     , ПрефиксацияОбъектовКлиентСервер.НомерНаПечать(Шапка.НомерДокумента));
	Параметры.Вставить("РуководительФИО"                    , ОтветственныеЛица.РуководительНаименование);
	Параметры.Вставить("НомерДатаСопроводительногоДокумента", ПредставлениеВходящегоДокумента);
	
	УчастникиПриемки = Новый Структура;
	УчастникиПриемки.Вставить("Получатель",       Шапка.Получатель);
	УчастникиПриемки.Вставить("Грузоотправитель", Шапка.Грузоотправитель);
	УчастникиПриемки.Вставить("Отправитель",      Шапка.Отправитель);
	
	Для Каждого Участник Из УчастникиПриемки Цикл
		
		Сведения = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Участник.Значение, Шапка.ДатаДокумента);
		
		Параметры.Вставить(Участник.Ключ                  , Участник.Значение);
		Параметры.Вставить(Участник.Ключ + "Представление", ФормированиеПечатныхФорм.ОписаниеОрганизации(Сведения));
		Параметры.Вставить(Участник.Ключ + "ИНН"          , Сведения.ИНН);
		Параметры.Вставить(Участник.Ключ + "ОКПО"         , Сведения.КодПоОКПО);
		Параметры.Вставить(Участник.Ключ + "КПП"          , Сведения.КПП);
		Параметры.Вставить(Участник.Ключ + "Адрес"        , Сведения.ЮридическийАдрес);
		
	КонецЦикла;
	
	Возврат Параметры;
	
КонецФункции

Процедура ПроверитьВыводСтроки(ТабличныйДокумент, Макет, МассивВыводимыхОбластей, ТекущаяОбласть, ЭтоПоследняяСтрока)
	
	МассивВыводимыхОбластей.Очистить();
	МассивВыводимыхОбластей.Добавить(ТекущаяОбласть);
	Если ЭтоПоследняяСтрока Тогда
		МассивВыводимыхОбластей.Добавить(Макет.ПолучитьОбласть("ИтогоРаздел9"));
	КонецЕсли;
	
	Если Не ОбщегоНазначения.ПроверитьВыводТабличногоДокумента(ТабличныйДокумент, МассивВыводимыхОбластей) Тогда
		ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		ТабличныйДокумент.Вывести(Макет.ПолучитьОбласть("ШапкаРаздел9"));
	КонецЕсли;
	
КонецПроцедуры

Функция МетаданныеМакета()
	
	Возврат Метаданные.Обработки.ПечатьАктПриемкиМатериалов0504220.Макеты.ПФ_MXL_АктПриемкиМатериалов0504220;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции

#КонецЕсли
