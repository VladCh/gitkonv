
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Если Параметры.Свойство("ПараметрыПодключения") Тогда
		ПараметрыСоединения = Параметры.ПараметрыПодключения;
	КонецЕсли;
	
	ИменаРеквизитов                   = СтруктураСоответствияНастройкиОтборовРеквизитамФормы();
	ИменаРеквизитовБазыКорреспондента = СтруктураСоответствияНастройкиОтборовКорреспондентаРеквизитамФормы();
	
	ОбменДаннымиСервер.ФормаНастройкиУзловПриСозданииНаСервере(ЭтотОбъект, Отказ);
	
	СтруктураОтбора = Новый Структура("Использовать", Истина);
	ОрганизацииБА.Загрузить(ОрганизацииБА.Выгрузить(СтруктураОтбора));
	Для Каждого Строка Из ОрганизацииБА Цикл
		Строка.Представление = Справочники.Организации.ПолучитьСсылку(Новый УникальныйИдентификатор(Строка.УникальныйИдентификаторСсылки));
	КонецЦикла;
	Элементы.УстановитьДатуЗапретаИзменений.Доступность = ПравоДоступа("Изменение", Метаданные.РегистрыСведений.ДатыЗапретаИзменения);
	
	МетаданныеОбъекта = Метаданные.ПланыОбмена.ОбменБольничнаяАптекаБухгалтерияГосударственногоУчреждения20;
	Для Каждого ЭлементСостава Из МетаданныеОбъекта.Состав Цикл
		Если ОбщегоНазначения.ЭтоДокумент(ЭлементСостава.Метаданные) Тогда
			ДоступныеДокументы.Добавить(ОбщегоНазначения.ИдентификаторОбъектаМетаданных(ЭлементСостава.Метаданные));
		КонецЕсли;
	КонецЦикла;
	
	КоличествоЭлементов = ВыгружаемыеДокументы.Количество();
	Для Индекс = 1 По КоличествоЭлементов Цикл
		
		Строка = ВыгружаемыеДокументы[КоличествоЭлементов - Индекс];
		Если Строка.Использовать Тогда
			Строка.Представление = Справочники.ИдентификаторыОбъектовМетаданных.ПолучитьСсылку(Новый УникальныйИдентификатор(Строка.УникальныйИдентификаторСсылки));
		КонецЕсли;
		
		Если Не Строка.Использовать Или ДоступныеДокументы.НайтиПоЗначению(Строка.Представление) = Неопределено Тогда
			ВыгружаемыеДокументы.Удалить(Строка);
		КонецЕсли;
		
	КонецЦикла;
	
	УстановитьВидимостьНаСервере();
	ОбновитьНаименованиеКомандФормы();
	ПолучитьОписаниеКонтекста();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	ОбновитьДанныеОбъекта(ВыбранноеЗначение);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	ОбменДаннымиКлиент.ФормаНастройкиПередЗакрытием(Отказ, ЭтотОбъект, ЗавершениеРаботы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	Если Не ЗаписатьИЗакрытьНаСервере() Тогда
		Возврат;
	КонецЕсли;
	
	ОбменДаннымиКлиент.ФормаНастройкиУзловКомандаЗакрытьФорму(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокВыбранныхОрганизаций(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "ОрганизацииБА";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "Представление";
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.Организации";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберите организации для отбора:'");
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокВыбранныхОрганизацийБГУ(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "ОрганизацииБГУ";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "УникальныйИдентификаторСсылки";
	ПараметрыФормы.ПараметрыВнешнегоСоединения            = ПараметрыСоединения;
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.Организации";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберите организации для отбора:'");
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокВыбранныхДокументов(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "ВыгружаемыеДокументы";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "Представление";
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.ИдентификаторыОбъектовМетаданных";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберете типы документов для отбора:'");
	
	ОтборСправочника = Новый Структура;
	ОтборСправочника.Вставить("РеквизитОтбора"   , "Ссылка");
	ОтборСправочника.Вставить("Условие"          , "В");
	ОтборСправочника.Вставить("ИмяПараметра"     , "ДоступныеДокументы");
	ОтборСправочника.Вставить("ЗначениеПараметра", ДоступныеДокументы);
	
	КоллекцияФильтров = Новый Массив;
	КоллекцияФильтров.Добавить(ОтборСправочника);
	
	ПараметрыФормы.КоллекцияФильтров = КоллекцияФильтров;
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокВыбранныхКонтрагентов(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "Контрагенты";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "УникальныйИдентификаторСсылки";
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.Контрагенты";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберете контрагентов для отбора:'");
	ПараметрыФормы.ПараметрыВнешнегоСоединения            = ПараметрыСоединения;
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

Процедура ПереключательОтправлятьНСИАвтоматическиПриИзмененииБА(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИПоНеобходимостиПриИзмененииБА(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИНикогдаПриИзмененииБА(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыОтправлятьАвтоматическиПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыОтправлятьВручнуюПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыНеОтправлятьПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ФлагИспользоватьОтборПоОрганизациямПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьОтборПоТипамДокументовПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИАвтоматическиПриИзмененииБГУ(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИВРучнуюПриИзмененииБГУ(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИНикогдаПриИзмененииБГУ(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ФлагИспользоватьОтборПоКонтрагентамПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

#Область ИменаРеквизитов

&НаСервере
Функция СтруктураСоответствияНастройкиОтборовРеквизитамФормы()
	
	СтруктураНастроек = Новый Структура;
	СтруктураНастроек.Вставить("ДатаНачалаВыгрузкиДокументов"      , "ДатаНачалаВыгрузкиДокументов");
	СтруктураНастроек.Вставить("РежимВыгрузкиСправочников"         , "РежимВыгрузкиСправочниковБА");
	СтруктураНастроек.Вставить("РежимВыгрузкиДокументов"           , "РежимВыгрузкиДокументов");
	СтруктураНастроек.Вставить("РежимВыгрузкиДвиженийНоменклатуры" , "РежимВыгрузкиДвиженийНоменклатуры");
	СтруктураНастроек.Вставить("ИспользоватьОтборПоОрганизациям"   , "ИспользоватьОтборПоОрганизациямБА");
	СтруктураНастроек.Вставить("Организации"                       , "ОрганизацииБА");
	СтруктураНастроек.Вставить("ИспользоватьОтборПоТипамДокументов", "ИспользоватьОтборПоТипамДокументов");
	СтруктураНастроек.Вставить("ВыгружаемыеДокументы"              , "ВыгружаемыеДокументы");
	
	Возврат СтруктураНастроек;
	
КонецФункции

&НаСервере
Функция СтруктураСоответствияНастройкиОтборовКорреспондентаРеквизитамФормы()
	
	СтруктураНастроек = Новый Структура;
	СтруктураНастроек.Вставить("РежимВыгрузкиСправочников"       , "РежимВыгрузкиСправочниковБГУ");
	СтруктураНастроек.Вставить("ИспользоватьОтборПоОрганизациям" , "ИспользоватьОтборПоОрганизациямБГУ");
	СтруктураНастроек.Вставить("Организации"                     , "ОрганизацииБГУ");
	СтруктураНастроек.Вставить("ИспользоватьОтборПоКонтрагентам" , "ИспользоватьОтборПоКонтрагентам");
	СтруктураНастроек.Вставить("Контрагенты"                     , "Контрагенты");
	
	Возврат СтруктураНастроек;
	
КонецФункции

#КонецОбласти // ИменаРеквизитов

#Область ОписаниеКонтекста

&НаСервере
Процедура ПолучитьОписаниеКонтекста()
	
	// Используется в помощнике настройки обмена для отображения значений настроек.
	ОписаниеКонтекста = (""
		+ СформироватьОписаниеПравилОтправкиБА()
		+ Символы.ПС + Символы.ПС
		+ СформироватьОписаниеПравилОтправкиБГУ());
	
КонецПроцедуры

&НаСервере
Функция СформироватьОписаниеПравилОтправкиБА()
	
	НастройкаОтборовНаУзле = Новый Структура;
	НастройкаОтборовНаУзле.Вставить("ДатаНачалаВыгрузкиДокументов"      , ДатаНачалаВыгрузкиДокументов);
	НастройкаОтборовНаУзле.Вставить("РежимВыгрузкиСправочников"         , РежимВыгрузкиСправочниковБА);
	НастройкаОтборовНаУзле.Вставить("РежимВыгрузкиДокументов"           , РежимВыгрузкиДокументов);
	НастройкаОтборовНаУзле.Вставить("ИспользоватьОтборПоОрганизациям"   , ИспользоватьОтборПоОрганизациямБА);
	НастройкаОтборовНаУзле.Вставить("Организации"                       , Новый Структура("Организация", ОрганизацииБА.Выгрузить().ВыгрузитьКолонку("Представление")));
	НастройкаОтборовНаУзле.Вставить("ИспользоватьОтборПоТипамДокументов", ИспользоватьОтборПоТипамДокументов);
	НастройкаОтборовНаУзле.Вставить("ВыгружаемыеДокументы"              , Новый Структура("ИдентификаторДокумента", ВыгружаемыеДокументы.Выгрузить().ВыгрузитьКолонку("Представление")));
	
	ТекстОписания = НСтр("ru = 'Правила отправки данных из информационной базы ""Больничная аптека"":'") + Символы.ПС;
	ТекстОписания = ТекстОписания + ПланыОбмена.ОбменБольничнаяАптекаБухгалтерияГосударственногоУчреждения20.ОписаниеОграниченийПередачиДанных(НастройкаОтборовНаУзле, ВерсияКорреспондента, Неопределено);
	
	Возврат ТекстОписания;
	
КонецФункции

&НаСервере
Функция СформироватьОписаниеПравилОтправкиБГУ()
	
	НастройкаОтборовНаУзле = Новый Структура;
	НастройкаОтборовНаУзле.Вставить("РежимВыгрузкиСправочников"      , РежимВыгрузкиСправочниковБГУ);
	НастройкаОтборовНаУзле.Вставить("ИспользоватьОтборПоОрганизациям", ИспользоватьОтборПоОрганизациямБГУ);
	НастройкаОтборовНаУзле.Вставить("Организации"                    , Новый Структура("Организация", ОрганизацииБГУ.Выгрузить().ВыгрузитьКолонку("Представление")));
	НастройкаОтборовНаУзле.Вставить("ИспользоватьОтборПоКонтрагентам", ИспользоватьОтборПоКонтрагентам);
	НастройкаОтборовНаУзле.Вставить("Контрагенты"                    , Новый Структура("Контрагент", Контрагенты.Выгрузить().ВыгрузитьКолонку("Представление")));
	
	ТекстОписания = НСтр("ru = 'Правила отправки данных из информационной базы ""Бухгалтерия государственного учреждения"":'") + Символы.ПС;
	ТекстОписания = ТекстОписания + ПланыОбмена.ОбменБольничнаяАптекаБухгалтерияГосударственногоУчреждения20.ОписаниеОграниченийПередачиДанныхБазыКорреспондента(НастройкаОтборовНаУзле, ВерсияКорреспондента, Неопределено);
	
	Возврат ТекстОписания;
	
КонецФункции

#КонецОбласти // ОписаниеКонтекста

#Область ФормированиеСпискаЭлементовОтбора

&НаКлиенте
Процедура ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы)
	
	ПараметрыФормы.Вставить("МассивВыбранныхЗначений", СформироватьМассивВыбранныхЗначений(ПараметрыФормы));
	
	ОткрытьФорму("ОбщаяФорма.ФормаВыбораДополнительныхУсловий", ПараметрыФормы, ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Функция ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию()
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ИмяЭлементаФормыДляЗаполнения"         , "");
	ПараметрыФормы.Вставить("ИмяРеквизитаЭлементаФормыДляЗаполнения", "");
	ПараметрыФормы.Вставить("ИмяТаблицыВыбора"                      , "");
	ПараметрыФормы.Вставить("ЗаголовокФормыВыбора"                  , "");
	ПараметрыФормы.Вставить("ПараметрыВнешнегоСоединения"           , Неопределено);
	ПараметрыФормы.Вставить("КоллекцияФильтров"                     , Неопределено);
	ПараметрыФормы.Вставить("ТолькоПросмотр"                        , ТолькоПросмотр);
	
	Возврат ПараметрыФормы;
	
КонецФункции

&НаСервере
Функция СформироватьМассивВыбранныхЗначений(ПараметрыФормы)
	
	ИмяЭлементаФормы          = ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения;
	ИмяРеквизитаЭлементаФормы = ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения;
	
	Возврат ЭтотОбъект[ИмяЭлементаФормы].Выгрузить(, ИмяРеквизитаЭлементаФормы).ВыгрузитьКолонку(ИмяРеквизитаЭлементаФормы);
	
КонецФункции

#КонецОбласти // ФормированиеСпискаЭлементовОтбора

&НаСервере
Процедура УстановитьВидимостьНаСервере()
	
	Если РежимВыгрузкиСправочниковБА = Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать
	   И РежимВыгрузкиДокументов = Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать Тогда
		Элементы.ГруппаСтраницыОтборПоОрганизациям.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоОрганизациямПустая;
	Иначе
		Элементы.ГруппаСтраницыОтборПоОрганизациям.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоОрганизациям;
	КонецЕсли;
	
	Если РежимВыгрузкиДокументов = Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать Тогда
		Элементы.ГруппаСтраницыОтборПоТипамДокументов.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоТипамДокументовПустая;
	Иначе
		Элементы.ГруппаСтраницыОтборПоТипамДокументов.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоТипамДокументов;
	КонецЕсли;
	
	Элементы.ДатаНачалаВыгрузкиДокументов.Доступность = (РежимВыгрузкиДокументов = Перечисления.РежимыВыгрузкиОбъектовОбмена.ВыгружатьПоУсловию);
	
	Если РежимВыгрузкиСправочниковБГУ = "Перечисление.РежимыВыгрузкиОбъектовОбмена.НеВыгружать" Тогда
		Элементы.ГруппаСтраницыОтборПоКонтрагентам.ТекущаяСтраница    = Элементы.ГруппаСтраницаОтборПоКонтрагентамПустая;
		Элементы.ГруппаСтраницыОтборПоОрганизациямБГУ.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоОрганизациямПустаяБГУ;
	Иначе
		Элементы.ГруппаСтраницыОтборПоКонтрагентам.ТекущаяСтраница    = Элементы.ГруппаСтраницаОтборПоКонтрагентам;
		Элементы.ГруппаСтраницыОтборПоОрганизациямБГУ.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоОрганизациямБГУ;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьНаименованиеКомандФормы()
	
	// Обновление заголовка команды выбора организаций для ограничения миграции.
	Если ОрганизацииБА.Количество() > 0 Тогда
		НовыйЗаголовокОрганизаций = СтрСоединить(ОрганизацииБА.Выгрузить().ВыгрузитьКолонку("Представление"), ", ");
	Иначе
		НовыйЗаголовокОрганизаций = НСтр("ru = 'Выбрать организации'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхОрганизаций.Заголовок = НовыйЗаголовокОрганизаций;
	
	// Обновление заголовка команды выбора типов документов для ограничения миграции.
	Если ВыгружаемыеДокументы.Количество() > 0 Тогда
		НовыйЗаголовокДокументы = СтрСоединить(ВыгружаемыеДокументы.Выгрузить().ВыгрузитьКолонку("Представление"), ", ");
	Иначе
		НовыйЗаголовокДокументы = НСтр("ru = 'Выбрать типы документов'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхДокументов.Заголовок = НовыйЗаголовокДокументы;
	
	// Обновление заголовка команды выбора организаций для ограничения миграции Корреспондента.
	Если ОрганизацииБГУ.Количество() > 0 Тогда
		НовыйЗаголовокОрганизаций = СтрСоединить(ОрганизацииБГУ.Выгрузить().ВыгрузитьКолонку("Представление"), ", ");
	Иначе
		НовыйЗаголовокОрганизаций = НСтр("ru = 'Выбрать организации'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхОрганизацийБГУ.Заголовок = НовыйЗаголовокОрганизаций;
	
	// Обновление заголовка команды выбора контрагентов для ограничения миграции Корреспондента.
	Если Контрагенты.Количество() > 0 Тогда
		НовыйЗаголовокКонтрагентов = СтрСоединить(Контрагенты.Выгрузить().ВыгрузитьКолонку("Представление"), ", ");
	Иначе
		НовыйЗаголовокКонтрагентов = НСтр("ru = 'Выбрать контрагентов'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхКонтрагентов.Заголовок = НовыйЗаголовокКонтрагентов;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеОбъекта(СтруктураПараметров)
	
	ЭтотОбъект[СтруктураПараметров.ИмяТаблицыДляЗаполнения].Очистить();
	
	СписокВыбранныхЗначений = ПолучитьИзВременногоХранилища(СтруктураПараметров.АдресТаблицыВоВременномХранилище);
	
	Если СписокВыбранныхЗначений.Количество() > 0 Тогда
		СписокВыбранныхЗначений.Колонки.Идентификатор.Имя = "УникальныйИдентификаторСсылки";
		СписокВыбранныхЗначений.Колонки.Добавить("Использовать");
		СписокВыбранныхЗначений.ЗаполнитьЗначения(Истина, "Использовать");
		ЭтотОбъект[СтруктураПараметров.ИмяТаблицыДляЗаполнения].Загрузить(СписокВыбранныхЗначений);
	КонецЕсли;
	
	ОбновитьНаименованиеКомандФормы();
	
КонецПроцедуры

&НаСервере
Функция ЗаписатьИЗакрытьНаСервере()
	
	Если Не ПроверитьЗаполнение() Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если РежимВыгрузкиСправочниковБА = Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать
	   И РежимВыгрузкиДокументов = Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать Тогда
		ИспользоватьОтборПоОрганизациямБА = Ложь;
	КонецЕсли;
	
	Если РежимВыгрузкиДокументов = Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать Тогда
		ИспользоватьОтборПоТипамДокументов = Ложь;
	КонецЕсли;
	
	Если Не ИспользоватьОтборПоОрганизациямБА И ОрганизацииБА.Количество() <> 0 Тогда
		ОрганизацииБА.Очистить();
	ИначеЕсли ОрганизацииБА.Количество() = 0 И ИспользоватьОтборПоОрганизациямБА Тогда
		ИспользоватьОтборПоОрганизациямБА = Ложь;
	КонецЕсли;
	
	Если Не ИспользоватьОтборПоТипамДокументов И ВыгружаемыеДокументы.Количество() <> 0 Тогда
		ВыгружаемыеДокументы.Очистить();
	ИначеЕсли ВыгружаемыеДокументы.Количество() = 0 И ИспользоватьОтборПоТипамДокументов Тогда
		ИспользоватьОтборПоТипамДокументов = Ложь;
	КонецЕсли;
	
	Если РежимВыгрузкиДокументов <> Перечисления.РежимыВыгрузкиОбъектовОбмена.ВыгружатьПоУсловию Тогда
		ДатаНачалаВыгрузкиДокументов = Дата(1,1,1,0,0,0);
	КонецЕсли;
	
	Если РежимВыгрузкиСправочниковБГУ = "Перечисления.РежимыВыгрузкиОбъектовОбмена.НеВыгружать" Тогда
		ИспользоватьОтборПоОрганизациямБГУ = Ложь;
		ИспользоватьОтборПоКонтрагентам    = Ложь;
	КонецЕсли;
	
	Если Не ИспользоватьОтборПоОрганизациямБГУ И ОрганизацииБГУ.Количество() <> 0 Тогда
		ОрганизацииБГУ.Очистить();
	ИначеЕсли ОрганизацииБГУ.Количество() = 0 И ИспользоватьОтборПоОрганизациямБГУ Тогда
		ИспользоватьОтборПоОрганизациямБГУ = Ложь;
	КонецЕсли;
	
	Если Не ИспользоватьОтборПоКонтрагентам И Контрагенты.Количество() <> 0 Тогда
		Контрагенты.Очистить();
	ИначеЕсли Контрагенты.Количество() = 0 И ИспользоватьОтборПоКонтрагентам Тогда
		ИспользоватьОтборПоКонтрагентам = Ложь;
	КонецЕсли;
	
	ПолучитьОписаниеКонтекста();
	
	Возврат Истина;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции
