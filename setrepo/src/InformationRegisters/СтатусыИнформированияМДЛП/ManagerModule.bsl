#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Функция возвращает структуру значений по умолчанию для документа для движений.
//
// Возвращаемое значение:
//  Структура - значения по умолчанию
//
Функция ЗначенияПоУмолчанию(Документ) Экспорт
	
	МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоСсылке(Документ);
	
	Значения = ИнтеграцияМДЛП.ДанныеПустойЗаписиРегистра(Метаданные.РегистрыСведений.СтатусыИнформированияМДЛП);
	Значения.Документ            = Документ;
	Значения.Статус              = МенеджерОбъекта.СтатусИнформированияПоУмолчанию();
	Значения.ДальнейшееДействие1 = МенеджерОбъекта.ДальнейшееДействиеПоУмолчанию();
	
	Возврат Значения;
	
КонецФункции

// Осуществляет запись в регистр по переданным данным.
//
// Параметры:
//  ДанныеЗаписи - данные для записи в регистр
//
Процедура ДобавитьЗапись(ДанныеЗаписи) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерЗаписи = РегистрыСведений.СтатусыИнформированияМДЛП.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи, ДанныеЗаписи);
	МенеджерЗаписи.Записать();
	
КонецПроцедуры

Функция РассчитатьСтатусы(ДокументСсылка, СтатусОбработки, Статусы) Экспорт
	
	Если СтатусОбработки = Перечисления.СтатусыОбработкиСообщенийМДЛП.Ошибка Тогда
		
		НовыйСтатус = Статусы.Ошибка;
		ДальнейшиеДействия = Статусы.ОшибкаДействия;
		
	ИначеЕсли СтатусОбработки = Перечисления.СтатусыОбработкиСообщенийМДЛП.Отклонено
	      Или СтатусОбработки = Перечисления.СтатусыОбработкиСообщенийМДЛП.ПринятоЧастично Тогда
		
		НовыйСтатус = Статусы.Отклонен;
		ДальнейшиеДействия = Статусы.ОтклоненДействия;
		
	Иначе
		
		НовыйСтатус = Статусы.Принят;
		ДальнейшиеДействия = Статусы.ПринятДействия;
		
	КонецЕсли;
	
	Возврат ВозвращаемоеЗначениеДальнейшиеДействияСтатус(НовыйСтатус, ДальнейшиеДействия);
	
КонецФункции

Функция РассчитатьСтатусыКПередаче(ДокументСсылка, НовыйСтатус) Экспорт
	
	ИспользоватьАвтоматическийОбмен = ПолучитьФункциональнуюОпцию("ИспользоватьАвтоматическуюОтправкуПолучениеДанныхМДЛП");
	
	ДальнейшиеДействия = Новый Массив;
	ДальнейшиеДействия.Добавить(Перечисления.ДальнейшиеДействияПоВзаимодействиюМДЛП.ВыполнитеОбмен);
	ДальнейшиеДействия.Добавить(Перечисления.ДальнейшиеДействияПоВзаимодействиюМДЛП.ОтменитеПередачуДанных);
	
	Возврат ВозвращаемоеЗначениеДальнейшиеДействияСтатус(НовыйСтатус, ДальнейшиеДействия);
	
КонецФункции

// Изменяет и возвращает статус документа.
//
// Параметры:
//  Документ - ДокументСсылка - Документ.
//  ПараметрыОбновления - Структура со свойствами:
//   * НовыйСтатус - ПеречисленияСсылка.СтатусыИнформированияМДЛП - Новый статус.
//   * ДальнейшееДействие - ПеречислениеСсылка.ДальнейшиеДействияПоВзаимодействиюМДЛП - Дальнейшее действие.
//
// Возвращаемое значение:
//  ПеречислениеСсылка.СтатусыИнформированияМДЛП - новый статус документа МДЛП.
//
Функция ОбновитьСтатус(Документ, ПараметрыОбновления) Экспорт
	
	Если ПараметрыОбновления.НовыйСтатус = Неопределено Тогда
		ВызватьИсключение НСтр("ru = 'Ошибка изменения статуса'");
	КонецЕсли;
	
	НовыйСтатус = Неопределено;
	СтарыйСтатус = Неопределено;
	
	Запись = РегистрыСведений.СтатусыИнформированияМДЛП.СоздатьМенеджерЗаписи();
	Запись.Документ = Документ;
	Запись.Прочитать();
	
	Записать = Ложь;
	Если Запись.Выбран() Тогда
		Если Запись.Статус <> ПараметрыОбновления.НовыйСтатус Тогда
			СтарыйСтатус = Запись.Статус;
			Запись.Статус = ПараметрыОбновления.НовыйСтатус;
			НовыйСтатус = Запись.Статус;
			Записать = Истина;
		КонецЕсли;
		Для Индекс = 1 По 3 Цикл
			Если Запись["ДальнейшееДействие" + Индекс] <> ПараметрыОбновления["ДальнейшееДействие" + Индекс] Тогда
				Запись["ДальнейшееДействие" + Индекс] = ПараметрыОбновления["ДальнейшееДействие" + Индекс];
				Записать = Истина;
			КонецЕсли;
		КонецЦикла;
	Иначе
		ЗаполнитьЗначенияСвойств(Запись, ЗначенияПоУмолчанию(Документ));
		ЗаполнитьЗначенияСвойств(Запись, ПараметрыОбновления);
		НовыйСтатус = Запись.Статус;
		Записать = Истина;
	КонецЕсли;
	
	Если Записать Тогда
		
		Запись.Записать();
		
		ИнтеграцияМДЛП.ПриИзмененииСтатусаДокумента(Документ, СтарыйСтатус, НовыйСтатус);
		
	КонецЕсли;
	
	Возврат НовыйСтатус;
	
КонецФункции

Функция СтатусыОбработки() Экспорт
	
	Статусы = Новый Структура;
	Статусы.Вставить("Принят");
	Статусы.Вставить("Ошибка");
	Статусы.Вставить("Отклонен");
	
	Статусы.Вставить("ОшибкаДействия"  , Новый Массив);
	Статусы.Вставить("ПринятДействия"  , Новый Массив);
	Статусы.Вставить("ОтклоненДействия", Новый Массив);
	
	Возврат Статусы;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ВозвращаемоеЗначениеДальнейшиеДействияСтатус(Статус, ДальнейшиеДействия)
	
	ВозвращаемоеЗначение = Новый Структура;
	ВозвращаемоеЗначение.Вставить("НовыйСтатус",         Статус);
	ВозвращаемоеЗначение.Вставить("ДальнейшееДействие1", Перечисления.ДальнейшиеДействияПоВзаимодействиюМДЛП.НеТребуется);
	ВозвращаемоеЗначение.Вставить("ДальнейшееДействие2", Перечисления.ДальнейшиеДействияПоВзаимодействиюМДЛП.НеТребуется);
	ВозвращаемоеЗначение.Вставить("ДальнейшееДействие3", Перечисления.ДальнейшиеДействияПоВзаимодействиюМДЛП.НеТребуется);
	
	Индекс = 1;
	Для Каждого ДальнейшееДействие Из ДальнейшиеДействия Цикл
		ВозвращаемоеЗначение["ДальнейшееДействие" + Индекс] = ДальнейшееДействие;
		Индекс = Индекс + 1;
	КонецЦикла;
	
	Возврат ВозвращаемоеЗначение;
	
КонецФункции

#КонецОбласти

#КонецЕсли