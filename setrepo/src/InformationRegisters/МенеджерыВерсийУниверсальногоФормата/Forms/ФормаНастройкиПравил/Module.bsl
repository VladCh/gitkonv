
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Если ТипЗнч(Параметры.Отбор) = Тип("Структура") И Параметры.Отбор.Свойство("ПланОбмена", ПланОбмена) Тогда
		СоздатьЭлементыФормыПоВерсиямФормата(ПланОбмена);
	Иначе
		ТекстСообщения = НСтр("ru = 'Форма не предназначена для непосредственного использования.'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,, Отказ);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	Для Каждого ПравилоКонвертации Из ПравилаКонвертации Цикл
		
		Если ПравилоКонвертации.ИсточникПравил = ИсточникПравилЗагруженныеИзФайла()
		   И ПустаяСтрока(ПравилоКонвертации.АдресДанныхОбработки) Тогда
			
			ТекстСообщения = НСтр("ru = 'Для версии формата %1 не загружен файл правил обмена.'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%1", ПравилоКонвертации.ВерсияФорматаОбмена);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,, Отказ);
			
		ИначеЕсли ПравилоКонвертации.ИсточникПравил = ИсточникПравилРежимОтладки()
				И ПустаяСтрока(ПравилоКонвертации.ИмяФайлаОтладки) Тогда
			
			ТекстСообщения = НСтр("ru = 'Для версии формата %1 не выбран файл правил обмена для отладки.'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%1", ПравилоКонвертации.ВерсияФорматаОбмена);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,, Отказ);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	Оповестить = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
	ОбщегоНазначенияКлиент.ПоказатьПодтверждениеЗакрытияФормы(Оповестить, Отказ, ЗавершениеРаботы,, ТекстПредупреждения);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	ЗаписатьНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьИзФайла(Команда)
	
	ИдентификаторПравила = ИдентификаторПравилаПоЭлементу(ПравилаКонвертации, ЭтотОбъект.ТекущийЭлемент);
	ЗагрузитьИзФайлаЗапуск(ИдентификаторПравила);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыгрузитьВФайл(Команда)
	
	ИдентификаторПравила = ИдентификаторПравилаПоЭлементу(ПравилаКонвертации, ЭтотОбъект.ТекущийЭлемент);
	ВыгрузитьВФайлНаКлиенте(ИдентификаторПравила);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура Подключаемый_ИсточникПравилПриИзменении(Элемент)
	
	ИдентификаторПравила = ИдентификаторПравилаПоЭлементу(ПравилаКонвертации, ЭтотОбъект.ТекущийЭлемент);
	ПравилоКонвертации = ПравилаКонвертации.НайтиПоИдентификатору(ИдентификаторПравила);
	Если ПравилоКонвертации <> Неопределено Тогда
		
		ПравилоКонвертации.Записывать = Истина;
		
		УстановитьОформлениеПоПравилуКонвертации(Элементы, ПравилоКонвертации);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ИмяФайлаОтладкиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ИдентификаторПравила = ИдентификаторПравилаПоЭлементу(ПравилаКонвертации, Элемент);
	ВыбратьФайлПравилДляОтладки(ИдентификаторПравила);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

#Область СозданиеФормы

&НаСервере
Процедура СоздатьЭлементыФормыПоВерсиямФормата(ПланОбмена)
	
	ВерсииФорматаОбмена          = ОбменДаннымиБольничнаяАптека.ВерсииФорматаОбмена();
	ВнешниеПравилаКонвертации    = РегистрыСведений.МенеджерыВерсийУниверсальногоФормата.ДанныеВерсийФормата(ПланОбмена,, УникальныйИдентификатор);
	ВнутренниеПравилаКонвертации = ОбменДаннымиБольничнаяАптека.ПоддерживаемыеВерсииФорматаОбмена();
	
	ВнешниеВерсииФормата = Новый Массив;
	Для Каждого ВерсияФормата Из ВерсииФорматаОбмена Цикл
		
		ПравилоКонвертации = ПравилаКонвертации.Добавить();
		ПравилоКонвертации.Поддерживается = ВнутренниеПравилаКонвертации.Получить(ВерсияФормата) <> Неопределено;
		
		ВнешнееПравило = ВнешниеПравилаКонвертации.Найти(ВерсияФормата, "ВерсияФорматаОбмена");
		
		Если ВнешнееПравило <> Неопределено Тогда
			
			ЗаполнитьЗначенияСвойств(ПравилоКонвертации, ВнешнееПравило);
			
			ПравилоКонвертации.ИсточникПравил = ?(ВнешнееПравило.РежимОтладки, ИсточникПравилРежимОтладки(), ИсточникПравилЗагруженныеИзФайла());
			
			Если ПравилаЗагруженыИзФайла(ПравилоКонвертации) Тогда
				ВнешниеВерсииФормата.Добавить(ВерсияФормата);
			КонецЕсли;
			
		Иначе
			
			ПравилоКонвертации.ВерсияФорматаОбмена = ВерсияФормата;
			
			ВнутреннееПравило = ВнутренниеПравилаКонвертации.Получить(ВерсияФормата);
			Если ВнутреннееПравило <> Неопределено Тогда
				ПравилоКонвертации.ИсточникПравил = ИсточникПравилТиповыеИзКонфигурации();
			Иначе
				ПравилоКонвертации.ИсточникПравил = ИсточникПравилНеПоддерживается();
			КонецЕсли;
			
		КонецЕсли;
		
		СоздатьЭлементыФормыПоПравилу(ПравилоКонвертации);
		ИзменитьЭлементыФормыПоПравилу(ПравилоКонвертации);
		УстановитьОформлениеПоПравилуКонвертации(Элементы, ПравилоКонвертации);
		
	КонецЦикла;
	
	ВнешниеВерсииФорматаДоИзменения = Новый ФиксированныйМассив(ВнешниеВерсииФормата);
	
КонецПроцедуры

&НаСервере
Процедура СоздатьЭлементыФормыПоПравилу(ПравилоКонвертации)
	
	ТекстовоеОписание = "
	|<Форма>
	|	<Элементы>
	|		<ГруппаФормы Имя='ГруппаВерсияФорматаОбмена_[ИдентификаторВерсииФормата]'>
	|			<Свойство Имя='Заголовок'>[ЗаголовокГруппыВерсииФормата]</Свойство>
	|			<Свойство Имя='Вид'>ОбычнаяГруппа</Свойство>
	|			<Свойство Имя='Поведение'>Свертываемая</Свойство>
	|			<Свойство Имя='ОтображениеУправления'>Картинка</Свойство>
	|			<Свойство Имя='Отображение'>ОбычноеВыделение</Свойство>
	|			<Свойство Имя='Группировка'>Вертикальная</Свойство>
	|			<ПолеФормы Имя='ИсточникПравил_[ИдентификаторВерсииФормата]'>
	|				<Свойство Имя='Вид'>ПолеПереключателя</Свойство>
	|				<Свойство Имя='ПутьКДанным'>ПравилаКонвертации[[ИндексПравилаКонвертации]].ИсточникПравил</Свойство>
	|				<Свойство Имя='ПоложениеЗаголовка'>Нет</Свойство>
	|				<Свойство Имя='ВидПереключателя'>Тумблер</Свойство>
	|				<Свойство Имя='ОдинаковаяШиринаКолонок'>Да</Свойство>
	|				<Свойство Имя='СписокВыбора'>
	|					<Значение Тип='СписокЗначений'>
	|						<Элемент Представление=""[ПредставлениеИсточникаТиповыхПравил]"">
	|							<Значение Тип='Строка'>[ЗначениеИсточникаТиповыхПравил]</Значение>
	|						</Элемент>
	|						<Элемент Представление=""[ПредставлениеИсточникаПравилНеПоддерживается]"">
	|							<Значение Тип='Строка'>[ЗначениеИсточникаПравилНеПоддерживается]</Значение>
	|						</Элемент>
	|						<Элемент Представление=""[ПредставлениеИсточникаПравилИзФайла]"">
	|							<Значение Тип='Строка'>[ЗначениеИсточникаПравилИзФайла]</Значение>
	|						</Элемент>
	|						<Элемент Представление=""[ПредставлениеИсточникаПравилРежимОтладки]"">
	|							<Значение Тип='Строка'>[ЗначениеИсточникаПравилРежимОтладки]</Значение>
	|						</Элемент>
	|					</Значение>
	|				</Свойство>
	|				<События>
	|					<ПриИзменении Действие='Подключаемый_ИсточникПравилПриИзменении'/>
	|				</События>
	|			</ПолеФормы>
	|			<ГруппаФормы Имя='СтраницыДанныеПравил_[ИдентификаторВерсииФормата]'>
	|				<Свойство Имя='Вид'>Страницы</Свойство>
	|				<Свойство Имя='ОтображениеСтраниц'>Нет</Свойство>
	|				<ГруппаФормы Имя='Страница[ЗначениеИсточникаТиповыхПравил]_[ИдентификаторВерсииФормата]'>
	|					<Свойство Имя='Вид'>Страница</Свойство>
	|					<ДекорацияФормы Имя='Декорация[ЗначениеИсточникаТиповыхПравил]_[ИдентификаторВерсииФормата]'>
	|					</ДекорацияФормы>
	|				</ГруппаФормы>
	|				<ГруппаФормы Имя='Страница[ЗначениеИсточникаПравилНеПоддерживается]_[ИдентификаторВерсииФормата]'>
	|					<ДекорацияФормы Имя='Декорация[ЗначениеИсточникаПравилНеПоддерживается]_[ИдентификаторВерсииФормата]'>
	|					</ДекорацияФормы>
	|				</ГруппаФормы>
	|				<ГруппаФормы Имя='Страница[ЗначениеИсточникаПравилИзФайла]_[ИдентификаторВерсииФормата]'>
	|					<Свойство Имя='Вид'>Страница</Свойство>
	|					<ГруппаФормы Имя='ИсточникФайл_[ИдентификаторВерсииФормата]'>
	|						<Свойство Имя='Вид'>ОбычнаяГруппа</Свойство>
	|						<Свойство Имя='Отображение'>Нет</Свойство>
	|						<Свойство Имя='Группировка'>Горизонтальная</Свойство>
	|						<Свойство Имя='ОтображатьЗаголовок'>Ложь</Свойство>
	|						<ПолеФормы Имя='ИмяФайла_[ИдентификаторВерсииФормата]'>
	|							<Свойство Имя='Вид'>ПолеВвода</Свойство>
	|							<Свойство Имя='ПутьКДанным'>ПравилаКонвертации[[ИндексПравилаКонвертации]].ИмяФайла</Свойство>
	|							<Свойство Имя='ПоложениеЗаголовка'>Нет</Свойство>
	|							<Свойство Имя='ТолькоПросмотр'>Истина</Свойство>
	|							<Свойство Имя='АвтоОтметкаНезаполненного'>Истина</Свойство>
	|							<Свойство Имя='АвтоМаксимальнаяШирина'>Нет</Свойство>
	|							<Свойство Имя='МаксимальнаяШирина'>65</Свойство>
	|						</ПолеФормы>
	|						<КнопкаФормы Имя='ЗагрузитьИзФайла_[ИдентификаторВерсииФормата]'>
	|							<Свойство Имя='Вид'>ОбычнаяКнопка</Свойство>
	|							<Свойство Имя='ИмяКоманды'>ЗагрузитьИзФайла</Свойство>
	|							<Свойство Имя='ПропускатьПриВводе'>Да</Свойство>
	|						</КнопкаФормы>
	|						<КнопкаФормы Имя='ВыгрузитьВФайл_[ИдентификаторВерсииФормата]'>
	|							<Свойство Имя='Вид'>ОбычнаяКнопка</Свойство>
	|							<Свойство Имя='ИмяКоманды'>ВыгрузитьВФайл</Свойство>
	|							<Свойство Имя='ПропускатьПриВводе'>Да</Свойство>
	|						</КнопкаФормы>
	|					</ГруппаФормы>
	|				</ГруппаФормы>
	|				<ГруппаФормы Имя='Страница[ЗначениеИсточникаПравилРежимОтладки]_[ИдентификаторВерсииФормата]'>
	|					<Свойство Имя='Вид'>Страница</Свойство>
	|					<Свойство Имя='Высота'>3</Свойство>
	|					<ПолеФормы Имя='ИмяФайлаОтладки_[ИдентификаторВерсииФормата]'>
	|						<Свойство Имя='Вид'>ПолеВвода</Свойство>
	|						<Свойство Имя='ПутьКДанным'>ПравилаКонвертации[[ИндексПравилаКонвертации]].ИмяФайлаОтладки</Свойство>
	|						<Свойство Имя='КнопкаВыбора'>Да</Свойство>
	|						<Свойство Имя='АвтоОтметкаНезаполненного'>Истина</Свойство>
	|						<Свойство Имя='АвтоМаксимальнаяШирина'>Нет</Свойство>
	|						<Свойство Имя='МаксимальнаяШирина'>73</Свойство>
	|						<События>
	|							<НачалоВыбора Действие='Подключаемый_ИмяФайлаОтладкиНачалоВыбора'/>
	|						</События>
	|					</ПолеФормы>
	|				</ГруппаФормы>
	|			</ГруппаФормы>
	|			<ПолеФормы Имя='ИнформацияОПравилах_[ИдентификаторВерсииФормата]'>
	|				<Свойство Имя='Вид'>ПолеВвода</Свойство>
	|				<Свойство Имя='ПутьКДанным'>ПравилаКонвертации[[ИндексПравилаКонвертации]].Информация</Свойство>
	|				<Свойство Имя='ТолькоПросмотр'>Истина</Свойство>
	|				<Свойство Имя='МногострочныйРежим'>Истина</Свойство>
	|				<Свойство Имя='Высота'>3</Свойство>
	|				<Свойство Имя='АвтоМаксимальнаяШирина'>Нет</Свойство>
	|				<Свойство Имя='МаксимальнаяШирина'>85</Свойство>
	|				<Свойство Имя='РастягиватьПоВертикали'>Нет</Свойство>
	|			</ПолеФормы>
	|		</ГруппаФормы>
	|	</Элементы>
	|</Форма>
	|";
	
	ПараметрыЗаполнения = Новый Структура;
	ПараметрыЗаполнения.Вставить("ИдентификаторВерсииФормата"  , ИдентификаторВерсииФормата(ПравилоКонвертации.ВерсияФорматаОбмена));
	ПараметрыЗаполнения.Вставить("ЗаголовокГруппыВерсииФормата", СтрЗаменить(НСтр("ru = 'Версия формата обмена ""%1""'"), "%1", ПравилоКонвертации.ВерсияФорматаОбмена));
	ПараметрыЗаполнения.Вставить("ИндексПравилаКонвертации"    , ПравилаКонвертации.Индекс(ПравилоКонвертации));
	
	ПараметрыЗаполнения.Вставить("ЗначениеИсточникаТиповыхПравил"     , ИсточникПравилТиповыеИзКонфигурации());
	ПараметрыЗаполнения.Вставить("ПредставлениеИсточникаТиповыхПравил", НСтр("ru = 'Типовые правила'"));
	
	ПараметрыЗаполнения.Вставить("ЗначениеИсточникаПравилНеПоддерживается"     , ИсточникПравилНеПоддерживается());
	ПараметрыЗаполнения.Вставить("ПредставлениеИсточникаПравилНеПоддерживается", НСтр("ru = 'Не использовать'"));
	
	ПараметрыЗаполнения.Вставить("ЗначениеИсточникаПравилИзФайла"     , ИсточникПравилЗагруженныеИзФайла());
	ПараметрыЗаполнения.Вставить("ПредставлениеИсточникаПравилИзФайла", НСтр("ru = 'Правила из файла'"));
	
	ПараметрыЗаполнения.Вставить("ЗначениеИсточникаПравилРежимОтладки"     , ИсточникПравилРежимОтладки());
	ПараметрыЗаполнения.Вставить("ПредставлениеИсточникаПравилРежимОтладки", НСтр("ru = 'Режим отладки'"));
	
	ТекстовоеОписание = СтроковыеФункцииКлиентСервер.ВставитьПараметрыВСтроку(ТекстовоеОписание, ПараметрыЗаполнения);
	
	ОписаниеЭлементов = УправляемаяФорма.ПрочитатьОписаниеФормыИзСтроки(ТекстовоеОписание);
	УправляемаяФорма.СоздатьЭлементы(ЭтотОбъект, ОписаниеЭлементов);
	
КонецПроцедуры

&НаСервере
Процедура ИзменитьЭлементыФормыПоПравилу(ПравилоКонвертации)
	
	ИдентификаторВерсии = ИдентификаторВерсииФормата(ПравилоКонвертации.ВерсияФорматаОбмена);
	
	ПолеИсточникПравил = Элементы.Найти("ИсточникПравил_" + ИдентификаторВерсии);
	СписокИсточникаПравил = ПолеИсточникПравил.СписокВыбора;
	Если ПравилоКонвертации.Поддерживается Тогда
		СписокИсточникаПравил.Удалить(СписокИсточникаПравил.НайтиПоЗначению(ИсточникПравилНеПоддерживается()));
	Иначе
		СписокИсточникаПравил.Удалить(СписокИсточникаПравил.НайтиПоЗначению(ИсточникПравилТиповыеИзКонфигурации()));
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // СозданиеФормы

#Область ЗаписьПравил

// Продолжение процедуры ПередЗакрытием
//
&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	ЗаписатьНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьНаКлиенте()
	
	ОчиститьСообщения();
	Если Не ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	Оповестить = Новый ОписаниеОповещения("ЗаписатьНаКлиентеЗавершение", ЭтотОбъект);
	ПоказатьПодтверждениеИзмененияВнешнихВерсийФормата(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ЗаписатьНаКлиенте
//
&НаКлиенте
Процедура ЗаписатьНаКлиентеЗавершение(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ <> КодВозвратаДиалога.ОК Тогда
		Возврат;
	КонецЕсли;
	
	ЗаписатьНаСервере();
	
	Оповестить("ИзменениеПравилОбменаЧерезУниверсальныйФормат");
	
	// Для механизма подтверждения закрытия формы.
	// (см. процедуру ПередЗакрытием).
	Модифицированность = Ложь;
	
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьНаСервере()
	
	Для Каждого ПравилоКонвертации Из ПравилаКонвертации Цикл
		
		Если Не ПравилоКонвертации.Записывать Тогда
			Продолжить;
		КонецЕсли;
		
		Запись = РегистрыСведений.МенеджерыВерсийУниверсальногоФормата.СоздатьМенеджерЗаписи();
		ЗаполнитьЗначенияСвойств(Запись, ПравилоКонвертации);
		Запись.ПланОбмена = ПланОбмена;
		
		Если ПравилаЗагруженыИзФайла(ПравилоКонвертации) Тогда
			
			ДвоичныеДанные = ПолучитьИзВременногоХранилища(ПравилоКонвертации.АдресДанныхОбработки);
			Запись.ХранилищеОбработки = Новый ХранилищеЗначения(ДвоичныеДанные, Новый СжатиеДанных(9));
			
			Запись.РежимОтладки = ПравилоКонвертации.ИсточникПравил = ИсточникПравилРежимОтладки();
			Запись.Записать();
			
		ИначеЕсли ПравилоКонвертации.ИсточникПравил = ИсточникПравилРежимОтладки() Тогда
			
			Запись.РежимОтладки = Истина;
			Запись.Записать();
			
		Иначе
			Запись.РежимОтладки = Ложь;
			Запись.Удалить();
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти // ЗаписьПравил

#Область ЗагрузкаПравил

&НаКлиенте
Процедура ЗагрузитьИзФайлаЗапуск(ИдентификаторПравила)
	
	ОповещениеОЗавершении = Новый ОписаниеОповещения("ЗагрузитьИзФайлаЗавершение", ЭтотОбъект, ИдентификаторПравила);
	
	ПравилоКонвертации = ПравилаКонвертации.НайтиПоИдентификатору(ИдентификаторПравила);
	Если ПравилоКонвертации = Неопределено Тогда
		ВыполнитьОбработкуОповещения(ОповещениеОЗавершении);
	КонецЕсли;
	
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("ОповещениеОЗавершении", ОповещениеОЗавершении);
	ДополнительныеПараметры.Вставить("ИмяФайла"             , ПравилоКонвертации.ИмяФайла);
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("Ключ", "ПередДобавлениемДополнительногоОтчетаИлиОбработки");
	
	Оповестить = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеПодтверждения", ЭтотОбъект, ДополнительныеПараметры);
	ОткрытьФорму("ОбщаяФорма.ПредупреждениеБезопасности", ПараметрыФормы,,,,, Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаЗапуск
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеПодтверждения(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ <> "Продолжить" Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеОЗавершении);
		Возврат;
	КонецЕсли;
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеУстановкиРасширенийРаботыСФайлами", ЭтотОбъект, ДополнительныеПараметры);
	ОбщегоНазначенияКлиент.ПоказатьВопросОбУстановкеРасширенияРаботыСФайлами(ОписаниеОповещения);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаПослеПодтверждения
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеУстановкиРасширенийРаботыСФайлами(РасширениеПодключено, ДополнительныеПараметры) Экспорт
	
	Если Не РасширениеПодключено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеОЗавершении);
		Возврат;
	КонецЕсли;
	
	Диалог = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
	Диалог.ПолноеИмяФайла = ДополнительныеПараметры.ИмяФайла;
	Диалог.Фильтр         = НСтр("ru = 'Внешние обработки (*.epf)|*.epf'");
	Диалог.Заголовок      = НСтр("ru = 'Выберите файл внешней обработки'");
	Диалог.ПроверятьСуществованиеФайла = Истина;
	
	Оповестить = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеВыбораФайла", ЭтотОбъект, ДополнительныеПараметры);
	Диалог.Показать(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаПослеУстановкиРасширенийРаботыСФайлами
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеВыбораФайла(ВыбранныеФайлы, ДополнительныеПараметры) Экспорт
	
	Если ВыбранныеФайлы = Неопределено Или ВыбранныеФайлы.Количество() = 0 Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеОЗавершении);
		Возврат;
	КонецЕсли;
	
	Файл = Новый Файл(ВыбранныеФайлы[0]);
	ДополнительныеПараметры.Вставить("Файл", Файл);
	
	ИнформацияОФайле = Новый Структура;
	ИнформацияОФайле.Вставить("ИмяФайла"      , Файл.Имя);
	ИнформацияОФайле.Вставить("ПолноеИмяФайла", Файл.ПолноеИмя);
	ДополнительныеПараметры.Вставить("ИнформацияОФайле", ИнформацияОФайле);
	
	Оповестить = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеПроверкиСуществования", ЭтотОбъект, ДополнительныеПараметры);
	Файл.НачатьПроверкуСуществования(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаПослеВыбораФайла
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеПроверкиСуществования(Существует, ДополнительныеПараметры) Экспорт
	
	Если Не Существует Тогда
		
		ПараметрыПредупреждения = Новый Структура;
		ПараметрыПредупреждения.Вставить("Текст"    , НСтр("ru = 'Файл не существует.'"));
		ПараметрыПредупреждения.Вставить("Заголовок", НСтр("ru = 'Обработка не получена'"));
		ВывестиПредупреждение(ПараметрыПредупреждения, ДополнительныеПараметры.ОповещениеОЗавершении);
		
		Возврат;
		
	КонецЕсли;
	
	Если ВРег(ДополнительныеПараметры.Файл.Расширение) <> ".EPF" Тогда
		
		ПараметрыПредупреждения = Новый Структура;
		ПараметрыПредупреждения.Вставить("Текст"    , НСтр("ru = 'Расширение файла не соответствует расширению внешней обработки (.epf).'"));
		ПараметрыПредупреждения.Вставить("Заголовок", НСтр("ru = 'Обработка не получена'"));
		ВывестиПредупреждение(ПараметрыПредупреждения, ДополнительныеПараметры.ОповещениеОЗавершении);
		
		Возврат;
		
	КонецЕсли;
	
	Оповестить = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеПолученияВремениИзменения", ЭтотОбъект, ДополнительныеПараметры);
	ДополнительныеПараметры.Файл.НачатьПолучениеВремениИзменения(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаПослеПроверкиСуществования
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеПолученияВремениИзменения(ВремяИзменения, ДополнительныеПараметры) Экспорт
	
	ИнформацияОФайле = ДополнительныеПараметры.ИнформацияОФайле;
	ИнформацияОФайле.Вставить("ВремяИзменения", ВремяИзменения);
	
	Оповестить = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеПолученияРазмераФайла", ЭтотОбъект, ДополнительныеПараметры);
	ДополнительныеПараметры.Файл.НачатьПолучениеРазмера(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаПослеПолученияВремениИзменения
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеПолученияРазмераФайла(Размер, ДополнительныеПараметры) Экспорт
	
	ИнформацияОФайле = ДополнительныеПараметры.ИнформацияОФайле;
	ИнформацияОФайле.Вставить("Размер", Размер);
	
	ПомещаемыеФайлы = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Новый ОписаниеПередаваемогоФайла(ИнформацияОФайле.ПолноеИмяФайла));
	
	Оповестить = Новый ОписаниеОповещения("ЗагрузитьИзФайлаПослеПомещенияФайла", ЭтотОбъект, ДополнительныеПараметры);
	НачатьПомещениеФайлов(Оповестить, ПомещаемыеФайлы, , Ложь, УникальныйИдентификатор);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаПослеПолученияРазмераФайла
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаПослеПомещенияФайла(ПомещенныеФайлы, ДополнительныеПараметры) Экспорт
	
	Если ПомещенныеФайлы = Неопределено Или ПомещенныеФайлы.Количество() = 0 Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеОЗавершении);
		Возврат;
	КонецЕсли;
	
	АдресДанныхОбработки = ПомещенныеФайлы[0].Хранение;
	
	ОписаниеМенеджера = ОписаниеМенеджераФормата(АдресДанныхОбработки);
	Если ОписаниеМенеджера.Информация = Неопределено Тогда
		
		ПараметрыПредупреждения = Новый Структура;
		ПараметрыПредупреждения.Вставить("Текст"    , ОписаниеМенеджера.ТекстОшибки);
		ПараметрыПредупреждения.Вставить("Заголовок", НСтр("ru = 'Обработка не подключена'"));
		ВывестиПредупреждение(ПараметрыПредупреждения, ДополнительныеПараметры.ОповещениеОЗавершении);
		
		Возврат;
		
	КонецЕсли;
	
	ИнформацияОФайле = ДополнительныеПараметры.ИнформацияОФайле;
	
	ДанныеОЗагрузке = Новый Структура("АдресДанныхОбработки, ИмяФайла, Информация");
	ДанныеОЗагрузке.АдресДанныхОбработки = АдресДанныхОбработки;
	ДанныеОЗагрузке.ИмяФайла             = ИнформацияОФайле.ИмяФайла;
	ДанныеОЗагрузке.Информация           = СформироватьИнформациюОМенеджереФормата(ИнформацияОФайле, ОписаниеМенеджера);
	
	ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеОЗавершении, ДанныеОЗагрузке);
	
КонецПроцедуры

// Продолжение процедуры ЗагрузитьИзФайлаЗапуск
//
&НаКлиенте
Процедура ЗагрузитьИзФайлаЗавершение(ДанныеОЗагрузке, ИдентификаторПравила) Экспорт
	
	Если ДанныеОЗагрузке = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ПравилоКонвертации = ПравилаКонвертации.НайтиПоИдентификатору(ИдентификаторПравила);
	
	ЗаполнитьЗначенияСвойств(ПравилоКонвертации, ДанныеОЗагрузке);
	
	ПравилоКонвертации.Записывать     = Истина;
	ПравилоКонвертации.ИсточникПравил = ИсточникПравилЗагруженныеИзФайла();
	
	ТекстОповещения = НСтр("ru = 'Файл внешней обработки загружен'");
	ПоказатьОповещениеПользователя(ТекстОповещения,, ПравилоКонвертации.ИмяФайла, БиблиотекаКартинок.ЗеленаяГалка);
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ОписаниеМенеджераФормата(АдресДанныхОбработки)
	
	ОписаниеМенеджера = Новый Структура("Информация, ТекстОшибки");
	
	Результат = РегистрыСведений.МенеджерыВерсийУниверсальногоФормата.ПолучитьМенеджерФорматаИзВременногоХранилища(АдресДанныхОбработки);
	
	Если Результат.Получен Тогда
		МетаданныеМенеджераФормата = Результат.МенеджерФормата.Метаданные();
		ОписаниеМенеджера.Информация = МетаданныеМенеджераФормата.Комментарий;
	Иначе
		ОписаниеМенеджера.ТекстОшибки = Результат.ТекстОшибки;
	КонецЕсли;
	
	Возврат ОписаниеМенеджера;
	
КонецФункции

&НаКлиенте
Функция СформироватьИнформациюОМенеджереФормата(ИнформацияОФайле, ОписаниеМенеджера)
	
	Информация = НСтр("ru = 'Внешний менеджер обмена через универсальный формат.
		|Путь к файлу: %1
		|Дата изменения: %2
		|Размер: %3 КБ'");
	
	ДатаИзменения = Формат(ИнформацияОФайле.ВремяИзменения, "ДЛФ=DT");
	РазмерКБ      = Цел(ИнформацияОФайле.Размер / 1024);
	
	Информация = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(Информация, ИнформацияОФайле.ПолноеИмяФайла, ДатаИзменения, РазмерКБ);
	
	ИнформацияОМенеджере = ОписаниеМенеджера.Информация;
	Если Не ПустаяСтрока(ИнформацияОМенеджере) Тогда
		Информация = Информация + Символы.ПС + Информация;
	КонецЕсли;
	
	Возврат Информация;
	
КонецФункции

#КонецОбласти // ЗагрузкаПравил

#Область ВыборПравилДляОтладки

&НаКлиенте
Процедура ВыбратьФайлПравилДляОтладки(ИдентификаторПравила)
	
	Оповестить = Новый ОписаниеОповещения("ВыбратьФайлПравилДляОтладкиЗавершение", ЭтотОбъект, ИдентификаторПравила);
	
	ПравилоКонвертации = ПравилаКонвертации.НайтиПоИдентификатору(ИдентификаторПравила);
	Если ПравилоКонвертации = Неопределено Тогда
		ВыполнитьОбработкуОповещения(Оповестить);
	КонецЕсли;
	
	Диалог = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Открытие);
	Диалог.ПолноеИмяФайла = ПравилоКонвертации.ИмяФайлаОтладки;
	Диалог.Фильтр         = НСтр("ru = 'Внешние обработки (*.epf)|*.epf'");
	Диалог.Заголовок      = НСтр("ru = 'Выберите файл внешней обработки'");
	Диалог.ПроверятьСуществованиеФайла = Истина;
	Диалог.Показать(Оповестить);
	
КонецПроцедуры

&НаКлиенте
Процедура ВыбратьФайлПравилДляОтладкиЗавершение(ВыбранныеФайлы, ИдентификаторПравила) Экспорт
	
	Если ВыбранныеФайлы = Неопределено Или ВыбранныеФайлы.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ПравилоКонвертации = ПравилаКонвертации.НайтиПоИдентификатору(ИдентификаторПравила);
	ПравилоКонвертации.ИмяФайлаОтладки = ВыбранныеФайлы[0];
	ПравилоКонвертации.Записывать      = Истина;
	ПравилоКонвертации.ИсточникПравил  = ИсточникПравилРежимОтладки();
	
	Модифицированность = Истина;
	
КонецПроцедуры

#КонецОбласти // ВыборПравилДляОтладки

#Область ВыгрузкаПравил

&НаКлиенте
Процедура ВыгрузитьВФайлНаКлиенте(ИдентификаторПравила)
	
	ПравилоКонвертации = ПравилаКонвертации.НайтиПоИдентификатору(ИдентификаторПравила);
	Если ПравилоКонвертации = Неопределено Или ПустаяСтрока(ПравилоКонвертации.АдресДанныхОбработки) Тогда
		ТекстОповещения = НСтр("ru = 'Файл обработки не выгружен'");
		Пояснение = НСтр("ru = 'Файл менеджера версии формата не обнаружен в программе.'");
		ПоказатьОповещениеПользователя(ТекстОповещения,, Пояснение, БиблиотекаКартинок.Предупреждение_32);
		Возврат;
	КонецЕсли;
	
	ТекстСообщения = НСтр("ru = 'Для выгрузки внешнего менеджера обмена через универсальный формат рекомендуется установить расширение для веб-клиента 1С:Предприятие.'");
	Оповестить = Новый ОписаниеОповещения("ВыгрузитьВФайлПродолжение", ЭтотОбъект, ПравилоКонвертации);
	ОбщегоНазначенияКлиент.ПоказатьВопросОбУстановкеРасширенияРаботыСФайлами(Оповестить, ТекстСообщения);
	
КонецПроцедуры

// Продолжение процедуры ВыгрузитьВФайлНаКлиенте
//
&НаКлиенте
Процедура ВыгрузитьВФайлПродолжение(Подключено, ПравилоКонвертации) Экспорт
	
	Если Не Подключено Тогда
		ПолучитьФайл(ПравилоКонвертации.АдресДанныхОбработки, ПравилоКонвертации.ИмяФайла, Истина);
		Возврат;
	КонецЕсли;
	
	Диалог = Новый ДиалогВыбораФайла(РежимДиалогаВыбораФайла.Сохранение);
	Диалог.ПолноеИмяФайла     = ПравилоКонвертации.ИмяФайла;
	Диалог.Фильтр             = НСтр("ru = 'Внешние обработки (*.epf)|*.epf'");
	Диалог.Заголовок          = НСтр("ru = 'Укажите файл'");
	
	Оповестить = Новый ОписаниеОповещения("ВыгрузитьВФайлПослеВыбораФайла", ЭтотОбъект, ПравилоКонвертации);
	Диалог.Показать(Оповестить);
	
КонецПроцедуры

// Продолжение процедуры ВыгрузитьВФайлПродолжение
//
&НаКлиенте
Процедура ВыгрузитьВФайлПослеВыбораФайла(ВыбранныеФайлы, ПравилоКонвертации) Экспорт
	
	Если ВыбранныеФайлы <> Неопределено Тогда
		
		ПолноеИмяФайла = ВыбранныеФайлы[0];
		ПолучаемыеФайлы = Новый Массив;
		ПолучаемыеФайлы.Добавить(Новый ОписаниеПередаваемогоФайла(ПолноеИмяФайла, ПравилоКонвертации.АдресДанныхОбработки));
		
		Оповестить = Новый ОписаниеОповещения("ВыгрузитьВФайлЗавершение", ЭтотОбъект, ПравилоКонвертации);
		НачатьПолучениеФайлов(Оповестить, ПолучаемыеФайлы, ПолноеИмяФайла, Ложь);
		
	КонецЕсли;
	
КонецПроцедуры

// Продолжение процедуры ВыгрузитьВФайлПослеВыбораФайла
//
&НаКлиенте
Процедура ВыгрузитьВФайлЗавершение(ПолученныеФайлы, ПравилоКонвертации) Экспорт
	
	Если ПолученныеФайлы <> Неопределено Тогда
		ТекстОповещения = НСтр("ru = 'Файл внешней обработки выгружен'");
		ПоказатьОповещениеПользователя(ТекстОповещения,, ПравилоКонвертации.ИмяФайла, БиблиотекаКартинок.ЗеленаяГалка);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти // ВыгрузкаПравил

#Область Прочее

&НаКлиентеНаСервереБезКонтекста
Функция ИдентификаторПравилаПоЭлементу(ПравилаКонвертации, Элемент)
	
	Если Элемент = Неопределено Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	ИдентификаторВерсии = Сред(Элемент.Имя, СтрНайти(Элемент.Имя, "_") + 1);
	ВерсияФорматаОбмена = ВерсияФорматаПоИдентификатору(ИдентификаторВерсии);
	
	НайденныеПравила = ПравилаКонвертации.НайтиСтроки(Новый Структура("ВерсияФорматаОбмена", ВерсияФорматаОбмена));
	
	Если НайденныеПравила.Количество() = 0 Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Возврат НайденныеПравила[0].ПолучитьИдентификатор();
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьОформлениеПоПравилуКонвертации(Элементы, ПравилоКонвертации)
	
	ИдентификаторВерсии = ИдентификаторВерсииФормата(ПравилоКонвертации.ВерсияФорматаОбмена);
	
	СтраницыДанныеПравил = Элементы.Найти("СтраницыДанныеПравил_" + ИдентификаторВерсии);
	СтраницаДанныеПравил = Элементы.Найти("Страница" + ПравилоКонвертации.ИсточникПравил + "_" + ИдентификаторВерсии);
	СтраницыДанныеПравил.ТекущаяСтраница = СтраницаДанныеПравил;
	
	ПолеИнформацияОПравилах = Элементы.Найти("ИнформацияОПравилах_" + ИдентификаторВерсии);
	СтраницаЗагруженныеИзФайла = Элементы.Найти("Страница" + ИсточникПравилЗагруженныеИзФайла() + "_" + ИдентификаторВерсии);
	ПолеИнформацияОПравилах.Видимость = СтраницыДанныеПравил.ТекущаяСтраница = СтраницаЗагруженныеИзФайла;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ИсточникПравилТиповыеИзКонфигурации()
	
	Возврат "ТиповыеИзКонфигурации";
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ИсточникПравилНеПоддерживается()
	
	Возврат "НеПоддерживается";
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ИсточникПравилЗагруженныеИзФайла()
	
	Возврат "ЗагруженныеИзФайла";
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ИсточникПравилРежимОтладки()
	
	Возврат "РежимОтладки";
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ИдентификаторВерсииФормата(ВерсияФорматаОбмена)
	
	Возврат СтрЗаменить(ВерсияФорматаОбмена, ".", "_");
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ВерсияФорматаПоИдентификатору(ИдентификаторВерсии)
	
	Возврат СтрЗаменить(ИдентификаторВерсии, "_", ".");
	
КонецФункции

&НаКлиенте
Процедура ПоказатьПодтверждениеИзмененияВнешнихВерсийФормата(ОповещениеОЗавершении)
	
	ИзмененныеВнешниеВерсииФормата = Новый Массив;
	Для Каждого ВнешняяВерсияФормата Из ВнешниеВерсииФорматаДоИзменения Цикл
		
		НайденныеПравила = ПравилаКонвертации.НайтиСтроки(Новый Структура("ВерсияФорматаОбмена", ВнешняяВерсияФормата));
		Если НайденныеПравила.Количество() = 0 Тогда
			Продолжить;
		КонецЕсли;
		
		ПравилоКонвертации = НайденныеПравила[0];
		Если ПравилаЗагруженыИзФайла(ПравилоКонвертации) Тогда
			Продолжить;
		КонецЕсли;
		
		ИзмененныеВнешниеВерсииФормата.Добавить(ВнешняяВерсияФормата);
		
	КонецЦикла;
	
	КоличествоИзмененныхВерсий = ИзмененныеВнешниеВерсииФормата.Количество();
	Если КоличествоИзмененныхВерсий = 1 Тогда
		
		Текст = НСтр("ru = 'Для версии формата обмена ""%1"" из программы будет удален менеджер, загруженный из файла.
			|Продолжить?'");
		Текст = СтрЗаменить(Текст, "%1", ИзмененныеВнешниеВерсииФормата[0]);
		
	ИначеЕсли КоличествоИзмененныхВерсий > 1 Тогда
		
		Текст = НСтр("ru = 'Для версий формата обмена ""%1"" из программы будут удалены менеджеры, загруженные из файлов.
			|Продолжить?'");
		Текст = СтрЗаменить(Текст, "%1", СтрСоединить(ИзмененныеВнешниеВерсииФормата, """, """));
		
	Иначе
		ВыполнитьОбработкуОповещения(ОповещениеОЗавершении, КодВозвратаДиалога.ОК);
		Возврат;
	КонецЕсли;
	
	Кнопки = Новый СписокЗначений;
	Кнопки.Добавить(КодВозвратаДиалога.ОК    , НСтр("ru = 'Продолжить'"));
	Кнопки.Добавить(КодВозвратаДиалога.Отмена, НСтр("ru = 'Отмена'"));
	
	ПоказатьВопрос(ОповещениеОЗавершении, Текст, Кнопки,, КодВозвратаДиалога.ОК);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПравилаЗагруженыИзФайла(ПравилоКонвертации)
	
	Возврат (ПравилоКонвертации.ИсточникПравил = ИсточникПравилЗагруженныеИзФайла()
		Или ПравилоКонвертации.ИсточникПравил = ИсточникПравилРежимОтладки())
		  И Не ПустаяСтрока(ПравилоКонвертации.АдресДанныхОбработки);
	
КонецФункции

&НаКлиенте
Процедура ВывестиПредупреждение(ПараметрыПредупреждения, ОповещениеОЗавершении)
	
	ОписаниеПредупреждения = Новый Структура("Заголовок, Текст", "", "");
	
	ЗаполнитьЗначенияСвойств(ОписаниеПредупреждения, ПараметрыПредупреждения);
	
	Если ПустаяСтрока(ОписаниеПредупреждения.Текст) Тогда
		ВыполнитьОбработкуОповещения(ОповещениеОЗавершении);
		Возврат;
	КонецЕсли;
	
	Кнопки = Новый СписокЗначений;
	Кнопки.Добавить(КодВозвратаДиалога.ОК, НСтр("ru = 'Закрыть'"));
	
	Оповестить = Новый ОписаниеОповещения("ВывестиПредупреждениеПродолжение", ЭтотОбъект, ОповещениеОЗавершении);
	ПоказатьВопрос(Оповестить, ОписаниеПредупреждения.Текст, Кнопки,, КодВозвратаДиалога.ОК, ОписаниеПредупреждения.Заголовок);
	
КонецПроцедуры

// Продолжение процедуры ВывестиПредупреждение.
//
&НаКлиенте
Процедура ВывестиПредупреждениеПродолжение(Ответ, ОповещениеОЗавершении) Экспорт
	
	ВыполнитьОбработкуОповещения(ОповещениеОЗавершении);
	
КонецПроцедуры

#КонецОбласти // Прочее

#КонецОбласти // СлужебныеПроцедурыИФункции
