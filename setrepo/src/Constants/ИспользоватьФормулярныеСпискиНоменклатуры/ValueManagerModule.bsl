#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС
#Область ПрограммныйИнтерфейс

Процедура ПриОпределенииЗависимостиКонстант(ТаблицаКонстант) Экспорт
	
	ИмяКонстанты = Метаданные().Имя;
	
	ОбщегоНазначенияБольничнаяАптека.ДобавитьЗависимостьКонстант(ТаблицаКонстант,
		ИмяКонстанты                                                 , Ложь,
		Метаданные.Константы.ИспользоватьБизнесПроцессыФормуляров.Имя, Ложь);
	
КонецПроцедуры

#КонецОбласти // ПрограммныйИнтерфейс

#КонецЕсли
